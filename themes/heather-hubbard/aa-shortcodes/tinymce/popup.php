<?php

// loads the shortcodes class, wordpress is loaded with it
require_once( 'shortcodes.class.php' );

// get popup type
$popup = trim( $_GET['popup'] );
$shortcode = new aa_shortcodes( $popup );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head></head>
<body>
<div id="aa-popup">

	<div id="aa-shortcode-wrap">
		
		<div id="aa-sc-form-wrap">
		
			<div id="aa-sc-form-head">
			
				<?php echo $shortcode->popup_title; ?>
			
			</div>
			<!-- /#aa-sc-form-head -->
			
			<form method="post" id="aa-sc-form">
			
				<table id="aa-sc-form-table">
				
					<?php echo $shortcode->output; ?>
					
					<tbody>
						<tr class="form-row">
							<?php if( ! $shortcode->has_child ) : ?><td class="label">&nbsp;</td><?php endif; ?>
							<td class="field"><a href="#" class="button-primary aa-insert">Insert Shortcode</a></td>							
						</tr>
					</tbody>
				
				</table>
				<!-- /#aa-sc-form-table -->
				
			</form>
			<!-- /#aa-sc-form -->
		
		</div>
		<!-- /#aa-sc-form-wrap -->
		
		<div class="clear"></div>
		
	</div>
	<!-- /#aa-shortcode-wrap -->

</div>
<!-- /#aa-popup -->

</body>
</html>