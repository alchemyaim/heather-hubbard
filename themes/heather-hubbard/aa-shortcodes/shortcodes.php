<?php
/*---------------------------------
-----   Button Shortcode    -----
------------------------------------*/

function aa_button_shortcode($atts, $content){
	$html = '<a class="button ' . $atts['style'] . '" href="' . $atts['link'] . '" target="' . $atts['target'] . '">' . $atts['label'] . '</a>';
	return $html;
}
add_shortcode('button', 'aa_button_shortcode');


/*---------------------------------
-----   Columns Shortcodes    -----
------------------------------------*/
function full_width_shortcode($atts, $content){
	$html = '<div class="full-width">' . do_shortcode($content) . '</div>';
	return $html;
}
function one_half_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="one-half' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function one_third_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="one-third' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function two_third_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="two-third' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function one_fourth_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="one-fourth' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function three_fourth_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="three-fourth' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function one_fifth_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="one-fifth' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function two_fifth_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="two-fifth' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function three_fifth_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="three-fifth' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function one_sixth_shortcode($atts, $content){
	$last = !empty($atts) ? ' ' . $atts[0] : '';
	$html = '<div class="one-sixth' . $last . '">' . do_shortcode($content) . '</div>';
	return $html;
}
function clear_shortcode($atts, $content){ 
	$html = '<div class="clear-fix"></div>'; 
	return $html;
}
add_shortcode('full_width', 'full_width_shortcode');
add_shortcode('one_half', 'one_half_shortcode');
add_shortcode('one_third', 'one_third_shortcode');
add_shortcode('two_third', 'two_third_shortcode');
add_shortcode('one_fourth', 'one_fourth_shortcode');
add_shortcode('three_fourth', 'three_fourth_shortcode');
add_shortcode('one_fifth', 'one_fifth_shortcode');
add_shortcode('two_fifth', 'two_fifth_shortcode');
add_shortcode('three_fifth', 'three_fifth_shortcode');
add_shortcode('one_sixth', 'one_sixth_shortcode');
add_shortcode('clear', 'clear_shortcode');

/*---------------------------------
	Alert Box Shortcode
------------------------------------*/

function aa_alert_box_shortcode($atts, $content){
	
	if ($atts['close'] !="yes") {;
	$html = '<div class="alertBox ' . $atts['style'] . '-box">' . $content . '';
	}
	else
	$html = '<div class="alertBox ' . $atts['style'] . '-box">' . $content . '<div class="alert-close"></div>';
	
	$html .='</div>';
	return $html;
}
add_shortcode('alert_box', 'aa_alert_box_shortcode');

/*---------------------------------
	Tabs Shortcode
------------------------------------*/
global $tabs_j;
function aa_tabs_shortcode($atts, $content){
	$html = '<div class="tab-container"><ul class="etabs ' . $atts['style'] . '">';

	global $tabs_j; $tabs_j = 0;
	$data = explode('<!-- cut out -->', do_shortcode($content));
	$i = 0; $titles = ''; $contents = '';

	foreach($data as $item){
		if($i++%2==0)
			$titles .= $item;
		else 
			$contents .= $item;
	}
	
	$html .= $titles . '</ul><div class="panel-container">' . $contents . '</div></div>';
	return $html;
}
function aa_tab_shortcode($atts, $content){
	global $tabs_j; $tabs_j++;
	$html = '<li class="tab"><a href="#tab-' . $tabs_j . '">' . $atts['title'] . '</a></li><!-- cut out --><div id="tab-' . $tabs_j . '" class="nop">' . do_shortcode($content) . '</div><!-- cut out -->';
	return $html;
}
add_shortcode('tabs', 'aa_tabs_shortcode');
add_shortcode('tab', 'aa_tab_shortcode');

/*---------------------------------
	Toggles Shortcode
------------------------------------*/

function aa_toggles_shortcode($atts, $content){
	$html = do_shortcode($content);
	return $html;
}
function aa_toggle_shortcode($atts, $content){
	$html = '<div class="toggle"><div class="toggle-title">' . $atts['title'] . '</div>';
	$html .= '<div class="togglebox"><div class="toggle-content">' . do_shortcode($content) . '</div></div></div>';
	return $html;
}
add_shortcode('toggles', 'aa_toggles_shortcode');
add_shortcode('toggle', 'aa_toggle_shortcode');

/* ------------------------
-----   Accordion    -----
------------------------------*/

if ( ! function_exists( 'accordion_function' ) ) {

	function accordion_function( $atts, $content ){

	    extract( shortcode_atts( array(
	        'el_class'  => '',
	        'type'		=> 'accordion',
	        'opened' 	=> '0'
	    ), $atts ) );

	    $html = '<div data-opened="' . $opened . '" class="accordion ' . $type . ' ' . ( $el_class != '' ? ' ' . $el_class : '' ) . ' clearfix">';

	    $html .= do_shortcode( $content );

	    $html .= '</div>';

	    return $html;

	}

	add_shortcode( 'accordion', 'accordion_function' );

}

if ( ! function_exists( 'accordion_section_function' ) ) {

	function accordion_section_function( $atts, $content ){

	    extract( shortcode_atts( array(
	        'title' => 'Section',
	    ), $atts ) );

	    $html = '<section>
	    	<div class="accordion-title">' . $title . '</div>
	    	<div class="accordion-content">' . do_shortcode( $content ) . '</div>
	    </section>';

	    return $html;

	}	

	add_shortcode( 'accordion_section', 'accordion_section_function' );

}


/* ------------------------
-----   Google Map    -----
------------------------------*/

/*Forked from "Very Simple Google Maps" Plugin*/
/* This section enables adding an very simple embeded Google Map with only a simple shortcode */
    function vsg_maps_shortcode($atts, $content = null) {
    extract(shortcode_atts(array(
    "align" => 'left',
    "width" => '100%',
    "height" => '350',
    "address" => '',
	"info_window" => 'A',
	"zoom" => '14',
	"companycode" => ''
    ), $atts));
	$query_string = 'q=' . urlencode($address) . '&cid=' . urlencode($companycode) . '&center=' . urlencode($address);
    return '<div class="vsg-map"><iframe class="google-map" align="'.$align.'" width="'.$width.'" height="'.$height.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?&'.htmlentities($query_string).'&output=embed&z='.$zoom.'&iwloc='.$info_window.'&visual_refresh=true"></iframe></div>';
    }
    add_shortcode("vsgmap", "vsg_maps_shortcode");

/*---------------------------------
	Social Icons Shortcode
------------------------------------*/

function aa_social_shortcode($atts, $content){
	$html = '<ul class="social-icons fa-ul">' . do_shortcode($content) . '</ul>';
	return $html;
}
function aa_link_shortcode($atts, $content){
	$html = '<li class="fa-li"><a href="' . $atts['href'] . '" target="' . $atts['target'] . '"><i class="fa ' . $atts['type'] . '"></i>
</a></li>';
	return $html;
}	
add_shortcode('social', 'aa_social_shortcode');
add_shortcode('link', 'aa_link_shortcode');

/*---------------------------------
-----   Video Shortcode    -----
------------------------------------*/

function aa_video_shortcode($atts, $content){
	if ($atts['source'] =="vimeo") {;
	$html='<div class="video-container"><iframe src="http://player.vimeo.com/video/'. $atts['id'] .'" width="960" height="540" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
	} else if ($atts['source'] =="youtube") {;
	$html='<div class="video-container"><iframe width="960" height="540" src="http://www.youtube.com/embed/'. $atts['id'] .'?rel=0&vq=hd1080;3&amp;autohide=1&amp;&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>';
	}
	return $html;
}
add_shortcode('embed-video', 'aa_video_shortcode');


/*---------------------------------
	Latest Projects Shortcode
------------------------------------*/

function aa_latest_projects_shortcode($atts, $content){
	$html = '<div class="clear-fix"></div><div id="portfolio" class="portfolio"><ul class="items latest-portfolio"><div class="gutter-sizer"></div>';

	global $post;
	
	$args = array( 'posts_per_page' => $atts['no'], 
		   'offset'=> 0,
			'post_type' => 'portfolio');
	$all_posts = new WP_Query($args);
	
	
	while($all_posts->have_posts()) : $all_posts->the_post();
	$desc = get_field('project_short_description', $post->ID);
	$icon = get_field('type_of_media', $post->ID);

		$html .= '<li class="item"><a href="' . get_permalink() . '">
			<div class="caption">
				<h3>' . get_the_title() . '</h3>
				<span class="short-desc">' .$desc. '</span>
				<span class="icon '.$icon.'"></span>
			</div>' . get_the_post_thumbnail($post->ID, 'portfolio-image') . '
		</a></li>';

	endwhile;

	$html .= '</ul></div><div class="clear-fix"></div>';
	return $html;
}
add_shortcode('latest_projects', 'aa_latest_projects_shortcode');

/*---------------------------------
	Latest Posts Shortcode
------------------------------------*/

function aa_latest_posts_shortcode($atts, $content){
	
	$html = '<div class="latest-blog-wrapper"><ul class="latest-blog">';
	
		global $post; 

		$all_posts = get_posts( array('numberposts' => $atts['no'] ));
		
		foreach($all_posts as $post) : setup_postdata($post);
		
	
			$html .= '<li class="latest-blog-'. $atts['no'] .'">
				<div class="latest-post-img">' . get_the_post_thumbnail($post->ID, 'recent-posts-image') . '</div>
					<h6><a href="' . get_permalink() . '">' . get_the_title() . '</a></h6>
					<p>'.new_excerpt(20).'</p>
					<br />
					<a href="' . get_permalink() . '" class="button button-primary">Read More</a>
					'
				.'</li>';
			
				
		endforeach;
	wp_reset_query();
			
	$html .= '</ul></div><div style="clear:both;"></div>';
	return $html;
	
	
	
}
add_shortcode('latest_posts', 'aa_latest_posts_shortcode');

/*---------------------------------
	Optin Form Shortcode
------------------------------------*/

function optin_shortcode($atts, $content){ 
	$form = get_field('form_code','option');
	$html = '<div class="optin-form-solo">'.$form.'</div>';
	return $html;
}
add_shortcode('optin', 'optin_shortcode');


/*---------------------------------
	Recent Products Shortcode
------------------------------------*/

function aa_recent_products_shortcode($atts) {
    global $woocommerce_loop, $woocommerce;

    extract(shortcode_atts(array(
        'per_page'  => '12',
        'columns'   => '4',
        'orderby' => 'date',
        'order' => 'desc'
    ), $atts));

    $meta_query = $woocommerce->query->get_meta_query();

    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page' => $per_page,
        'orderby' => $orderby,
        'order' => $order,
        'meta_query' => $meta_query
    );

    ob_start();

    $products = new WP_Query( $args );
    $woocommerce_loop['columns'] = $columns;

    if ( $products->have_posts() ) : ?>
        <?php woocommerce_product_loop_start(); ?>
        <?php while ( $products->have_posts() ) : $products->the_post(); ?>
        <?php woocommerce_get_template_part( 'content', 'product' ); ?>
        <?php endwhile; // end of the loop. ?>
        <?php woocommerce_product_loop_end(); ?>
            
    <?php endif;
    wp_reset_postdata();
    return '<div class="recent-products"><div class="woocommerce">' . ob_get_clean() . '</div></div>';
 }
 add_shortcode('custom_recent_products','aa_recent_products_shortcode');


?>