<?php
/**
 * Index Page
 * 
 */

get_header();
?>
<style>.blog-intro{margin-bottom:50px;}</style>
<?php //White Nav
	if(get_field('white_nav', 26) == "true"){?>
		<style>#linkbar a{color:#fff;}#slideout-bar,#slideout-bar:before,#slideout-bar:after{background-color: #fff;</style>
<?php } ?>

<?php //Hero Header
	if(get_field('header', 26) == "true" && get_field('header_type', 26) == "hero") { 
	
	//var
	$hero_ht = get_field('hero_header_height', 26);
	$bg_hero_img = get_field('header_image', 26);
	$bg_hero_text = get_field('header_text', 26);	
?>

<!-- START HERO HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, 0.10), rgba(0,0,0, 0.10)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero', 26) ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<?php //Hero Slider
if(get_field('header', 26) == "true" && get_field('header_type', 26) == "slider") { 
	
	//var
	$hero_ht = get_field('hero_header_height', 26);
?>

<!-- START SLIDER HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<?php // check if the repeater field has rows of data
if( have_rows('header_slider') ): ?>
<section id="hero-slider">
<?php	// loop through the rows of data
    while ( have_rows('header_slider') ) : the_row(); 

	//var
	$bg_slider_img = get_sub_field('slider_background_image', 26);
	$bg_slider_text = get_sub_field('slider_text', 26);	
?>

<div class="slider-header slide" style="background-image:linear-gradient(rgba(0,0,0, 0.10), rgba(0,0,0, 0.10)),url(<?php echo $bg_slider_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">

	<div class="container">
		<div class="slider-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_sub_field('text_position_and_width') ?>">
			<div class="slider-text"><?php echo $bg_slider_text; ?></div>
		</div>
	</div>
</div>

<?php endwhile; ?>
</section>
<?php else :
    // no rows found
endif; ?>

<!-- END SLIDER HEADER -->
<?php } ?>


<?php //Hero Video Header
	if(get_field('header', 26) == "true" && get_field('header_type', 26) == "video") { 
	
	//var
	$hero_ht = get_field('hero_header_height', 26);
	$vid_hero_poster = get_field('header_poster', 26);
	$vid_hero_video = get_field('header_video', 26);
	$vid_hero_video_url = get_field('header_video_external_url', 26);
	$vid_hero_loop = get_field('header_video_loop', 26);
	$vid_hero_mute = get_field('header_video_mute', 26);
	$bg_hero_text = get_field('header_text', 26);
		
		if(get_field('header_video_loop', 26) == 1){ 
			$vid_loop = "true";
		}
		if(get_field('header_video_loop', 26) == 0){
			$vid_loop = "false";
		}
		if(get_field('header_video_mute', 26) == 1){ 
			$vid_mute = "true";
		}
		if(get_field('header_video_mute', 26) == 0){
			$vid_mute = "false";
		}
?>

<!-- START VIDEO HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<section class="hero-header-vid" data-vide-bg="mp4: <?php if( !empty($vid_hero_video) ){ echo $vid_hero_video;} elseif( !empty($vid_hero_video_url) ){echo $vid_hero_video_url;} ?>, poster: <?php echo $vid_hero_poster['url']; ?>" data-vide-options="posterType: png, loop: <?php echo $vid_loop; ?>, muted: <?php echo $vid_mute; ?>, position: 50% 50%">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero', 26) ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END VIDEO HEADER -->
<?php } ?>

<section class="blog-intro">
<div class="container">
<div class="row">
<div class="twelve columns">
<?php the_field('blog_intro_text', 26); ?>
	</div>
	</div>
	</div>
</section>

<?php //Remove Sidebar
if(get_field('blog_roll_page_sidebar','option') == "true"){ ?>
<style>.content-area{width:100% !important;}.aside{display:none;}</style>
<?php } ?>

<div class="container">
<article>
	<div class="content-area">
		<?php if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
				
    <div class="post-preview">
  <h2 class="post-title"><a href="<?php the_permalink(); ?>">
    <?php the_title(); ?>
    </a></h2>
    <div class="post-meta"><span class="post-date"><?php the_time('F j, Y'); ?></span></div>
    <div class="featured blog"><?php the_post_thumbnail('full'); ?></div>	
		<div class="post-excerpt"><?php the_excerpt(__('new_excerpt_length')); ?></div>
  <div class="read-more"><a href="<?php the_permalink(); ?>" class="button button-primary">read more</a></div>
</div>

			<?php endwhile; ?>
           
            <?php get_template_part( '_template-parts/part', 'navigation' ); ?>

		<?php else : ?>

			<h1 class="heading">Not Found</h1>
			<p class="center">Sorry, but you are looking for something that isn't here.</p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div>
</article>

<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
