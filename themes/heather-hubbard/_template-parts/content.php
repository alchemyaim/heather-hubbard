<?php
/**
 * Template part for displaying posts.
 *
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="post-body entry-content">
    	
        
  <div class="post-preview">
    <h2 class="post-title"><a href="<?php the_permalink(); ?>">
      <?php the_title(); ?>
      </a></h2>
    <div class="post-meta"><span class="post-date"><?php the_time('F j, Y'); ?></span></div>
    <div class="featured blog"><?php the_post_thumbnail('full'); ?></div>	
    <?php the_content(); ?>
	
  </div>    

<div class="post-footer">
	<div class="share-buttons">
     <?php 
		$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		$subject =  get_the_title();
		$body = get_permalink();
		$desc = get_the_excerpt();
		
	?>
		<div class="share-text">Share this:</div>
       <div class="fshare"><a href="https://www.facebook.com/dialog/feed?app_id=184683071273&link=<?php the_permalink() ?>&picture=<?php echo $feat_image; ?>&name=<?php echo the_title (); ?>&caption=%20&description=<?php echo $desc; ?>&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" target="_blank"><i class="fa fa-facebook"></i></a></div>
       
       <div class="tshare"><a href="http://twitter.com/home?status=Currently reading: <?php the_title ();?><?php echo get_settings('home'); ?>/?p=<?php the_ID(); ?>" target="_blank"><i class="fa fa-twitter"></i></a></div>
       
       <div class="pshare"><a href="//www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>&media=<?php echo $feat_image; ?>&description=<?php the_title(); ?> - <?php echo $desc; ?>" data-pin-do="buttonBookmark" target="_blank" ><i class="fa fa-pinterest-p"></i></a></div>
       
       <div class="mshare"><a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_title(); ?> - <?php the_permalink(); ?>"><i class="fa fa-envelope-o"></i></a></div>
       
       <div class="comment-link"><a href="<?php the_permalink(); ?>/#respond"><i class="fa fa-comment" aria-hidden="true"></i></a></div>
      
    </div>
</div>

<!--BEGIN AUTHOR BIO-->
		<div class='clearfix' id='about_author'>
			<?php echo get_avatar( get_the_author_meta('email'), '150' ); ?>
			<div class='author_text'>
				<h5>Professional Bio</h5>
				<p class="author-description"><?php the_author_meta('description'); ?></p>
				<div class='clear'></div>	
			</div>
		</div>
        	<!--END AUTHOR BIO-->

</div>

</article> <!--POST-->
