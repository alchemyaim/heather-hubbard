<?php
/*---------------------------------------------------------
	Template Name: Flexible Content Page
------------------------------------------------------------*/
	get_header();
?>

<?php //White Nav
	if(get_field('white_nav') == "true"){?>
		<style>#linkbar a{color:#fff;}#slideout-bar,#slideout-bar:before,#slideout-bar:after{background-color: #fff;</style>
<?php } ?>

<?php //Hero Header
	if(get_field('header') == "true" && get_field('header_type') == "hero") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, 0.10), rgba(0,0,0, 0.10)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<?php //Hero Slider
if(get_field('header') == "true" && get_field('header_type') == "slider") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
?>

<!-- START SLIDER HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<?php // check if the repeater field has rows of data
if( have_rows('header_slider') ): ?>
<section id="hero-slider">
<?php	// loop through the rows of data
    while ( have_rows('header_slider') ) : the_row(); 

	//var
	$bg_slider_img = get_sub_field('slider_background_image');
	$bg_slider_text = get_sub_field('slider_text');	
?>

<div class="slider-header slide" style="background-image:linear-gradient(rgba(0,0,0, 0.10), rgba(0,0,0, 0.10)),url(<?php echo $bg_slider_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">

	<div class="container">
		<div class="slider-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_sub_field('text_position_and_width') ?>">
			<div class="slider-text"><?php echo $bg_slider_text; ?></div>
		</div>
	</div>
</div>

<?php endwhile; ?>
</section>
<?php else :
    // no rows found
endif; ?>

<!-- END SLIDER HEADER -->
<?php } ?>


<?php //Hero Video Header
	if(get_field('header') == "true" && get_field('header_type') == "video") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$vid_hero_poster = get_field('header_poster');
	$vid_hero_video = get_field('header_video');
	$vid_hero_video_url = get_field('header_video_external_url');
	$vid_hero_loop = get_field('header_video_loop');
	$vid_hero_mute = get_field('header_video_mute');
	$bg_hero_text = get_field('header_text');
		
		if(get_field('header_video_loop') == 1){ 
			$vid_loop = "true";
		}
		if(get_field('header_video_loop') == 0){
			$vid_loop = "false";
		}
		if(get_field('header_video_mute') == 1){ 
			$vid_mute = "true";
		}
		if(get_field('header_video_mute') == 0){
			$vid_mute = "false";
		}
?>

<!-- START VIDEO HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<section class="hero-header-vid" data-vide-bg="mp4: <?php if( !empty($vid_hero_video) ){ echo $vid_hero_video;} elseif( !empty($vid_hero_video_url) ){echo $vid_hero_video_url;} ?>, poster: <?php echo $vid_hero_poster['url']; ?>" data-vide-options="posterType: png, loop: <?php echo $vid_loop; ?>, muted: <?php echo $vid_mute; ?>, position: 50% 50%">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END VIDEO HEADER -->
<?php } ?>



<?php //Flexible Content
// check if the flexible content field has rows of data
if( have_rows('flexible_content_fields') ):

 	// loop through the rows of data
    while ( have_rows('flexible_content_fields') ) : the_row();

		// Callout Box Content Section
        if( get_row_layout() == 'callout_box_section' ): 
	
	//var
	$bg = get_sub_field('section_background_color'); ?>
	
	<section class="callout-box-content <?php echo $bg; ?>">
		<div class="container">
			<div class="row">
				<div class="twelve columns">
					<?php the_sub_field('callout_header_text'); ?>
				</div>
			</div>
			<div class="row">
			<?php	// check if the nested repeater field has rows of data
        	if( have_rows('callout_boxes') ):

			 	echo '<div class="callout-box-container">';

			 	// loop through the rows of data
			    while ( have_rows('callout_boxes') ) : the_row();

					$box_txt = get_sub_field('callout_text');
					$icon = get_sub_field('icon');

					echo '<div class="one-third"><div class="callout-box">' .$icon. '' .$box_txt. '</div></div>';

				endwhile;

				echo '</div>';

			endif; ?>
			</div>
		</div>
	</section>
		
<?php endif;
	
	
	// About Section
    	if( get_row_layout() == 'about_section' ): 
	
	//var
	$about_txt = get_sub_field('about_text');
	$about_img = get_sub_field('about_image');
	?>
	
	<section class="about-section">
	<?php if(get_sub_field('image_position') == "left") { ?>
		
 		<div class="image" style="background-image:url(<?php echo $about_img['url']; ?>);"> </div>
  			<div class="item-content">
        		<?php echo $about_txt; ?>
      		</div>	
	<?php } ?>

	<?php if(get_sub_field('image_position') == "right") { ?>
	
		<style>.about-section .image-left{left:auto;right:0;}.about-section .item-content.txt-right{float:left;}</style>
		
		<div class="image image-left" style="background-image:url(<?php echo $about_img['url']; ?>);"> </div>
       		<div class="item-content txt-right">
        		<?php echo $about_txt; ?>
      		</div>	
 
	<?php } ?>
			
	</section>
		
<?php endif;
	
	
	// Team Section
        if( get_row_layout() == 'team_section' ): 
	
	//var
	$team_btn = get_sub_field('team_page_button_label'); 
	$team_lnk = get_sub_field('team_page_button_link');
	
	?>
	
	<section class="team-section">
		<div class="container">
			<div class="row">
				<div class="twelve columns">
					<?php the_sub_field('team_header_text'); ?>
				</div>
			</div>
			<?php
			// check if the nested repeater field has rows of data
        	if( have_rows('team') ): $i = 0;

			 	echo '<ul class="team-container wrap">';

			 	// loop through the rows of data
			    while ( have_rows('team') ) : the_row(); $i++;

					//var
					//$team_img = get_sub_field('team_member_image');
					$attachment_id = get_sub_field('team_member_image');
					$size = "team-image"; // (thumbnail, medium, large, full or custom size)
    				$team_img = wp_get_attachment_image_src( $attachment_id, $size );
					// url = $image[0];
					// width = $image[1];
					// height = $image[2];
			
					$team_name = get_sub_field('team_member_name');
					$team_title = get_sub_field('team_member_title');
					$team_bio = get_sub_field('team_member_bio');
			?>

					<li class="team-member"><a href="javascript:void(0);" id="bio-trigger-<?php echo $i; ?>">
						<img src="<?php echo $team_img[0]; ?>" />
							<span class="team-name"><?php echo $team_name; ?></span>
							<span class="team-title"><?php echo $team_title; ?></span>
						 </a></li>
			
			<style>
			#bio-pop.tm-<?php echo $i; ?>{background-color:#f5f5f5;border:thin solid #eee;position:fixed;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);width:770px;max-width:95%;z-index:-1;opacity:0;transition:.2s ease opacity}
			#bio-pop.tm-<?php echo $i; ?>.open {z-index:100;opacity:1;}
			#bio-close-<?php echo $i; ?>{position:absolute;top:20px;right:20px;height:15px;width:15px;text-decoration:none}#bio-close-<?php echo $i; ?>:before{content:"\f00d";font-family:FontAwesome;font-size:2rem}
			</style>
			
			<div id="bio-pop" class="tm-<?php echo $i; ?>">
      			<div class="inner">
        			<a href="#" id="bio-close-<?php echo $i; ?>"></a>
        			<img src="<?php echo $team_img[0]; ?>" />
        			<h2><?php echo $team_name; ?></h2>
		  			<span class="bio-title"><?php echo $team_title; ?></span>
          		<section>
            		<div>
             			<?php echo $team_bio; ?>
            		</div>
          		</section>
               
              </div>
    	</div>
    	
    
			<script>
				jQuery(document).ready(function ($) {
					"use strict";
					$('#bio-trigger-<?php echo $i; ?>, #bio-close-<?php echo $i; ?>').on('click', function(event){
				event.preventDefault();
				document.getElementById('fade').style.display='block';
				// create menu variable
    			var bioPop = $('#bio-pop.tm-<?php echo $i; ?>');
				var insideAnchor = $('#bio-pop.tm-<?php echo $i; ?> section a');
    			// toggle open class

				if (bioPop.hasClass("open")) {
					bioPop.removeClass("open");
					document.getElementById('fade').style.display='none';
				} else {
					bioPop.addClass("open");
				}
    			});
				});
			</script>
				
	<?php endwhile; ?>
			</div>
	<?php endif; ?>
	
		<?php 
			//var
			$team_btn_link = get_sub_field('team_page_button_link');
			$team_btn_label = get_sub_field('team_page_button_label');

		if( !empty($team_btn_label) ): ?>
			<div class="center"><a href="<?php echo $team_btn_link; ?>" class="button"><?php echo $team_btn_label; ?></a></div>
			<?php endif; ?>
			
		</div>
	</section>
	<div id="fade" class="white_overlay"></div>
		
<?php endif;
	
														 
// latest Posts Section
        if( get_row_layout() == 'latest_blog_section' ): 
	
	//var
	$bg = get_sub_field('section_background_color'); 
	$post_ct = get_sub_field('post_count_display'); ?>
	
	<section class="latest-posts-section <?php echo $bg; ?>">
		<div class="container">
			
				<div class="twelve columns">
					<?php the_sub_field('latest_blog_header_text'); ?>
				</div>
			
			<?php if(get_sub_field('post_count_display') == "1") { ?>
			<style>.latest-blog-wrapper li {width: 100%;float: none;margin-right: 0;}.latest-post-img{float:left;margin-right: 25px;}</style>
			<?php } ?>
			<?php if(get_sub_field('post_count_display') == "2") { ?>
			<style>.latest-blog-wrapper li {width: 49.2%;}</style>
			<?php } ?>
			<?php if(get_sub_field('post_count_display') == "4") { ?>
			<style>.latest-blog-wrapper li {width: 23.8%;}</style>
			<?php } ?>
			
				<?php echo do_shortcode('[latest_posts no="'.$post_ct.'" /]'); ?>	
				
			<div style="clear:both;"></div>
			<div class="center"><a href="<?php the_sub_field('more_posts_button_link'); ?>" class="button more-btn"><?php the_sub_field('more_posts_button_label'); ?></a></div>
			
		</div>
	</section>
		
<?php endif;	
	
	
	// Recent Products Section
        if( get_row_layout() == 'recent_products_section' ): 
	
	//var
	$prod_ct = get_sub_field('product_count_display'); ?>
	
	<section class="recent-products-section">
		<div class="container">
			
				<div class="twelve columns">
					<?php the_sub_field('recent_products_header_text'); ?>
				</div>
			
			
				<?php echo do_shortcode('[custom_recent_products per_page="'.$prod_ct.'" /]'); ?>	
				
			
			<div class="center"><a href="<?php the_sub_field('more_products_button_link'); ?>" class="button"><?php the_sub_field('more_products_button_label'); ?></a></div>
			
		</div>
	</section>
		
<?php endif;
	
	
	// Recent Projects Section
        if( get_row_layout() == 'latest_projects_section' ): 
	
	//var
	$proj_ct = get_sub_field('projects_count_display'); ?>
	
	<section class="latest-projects-section">
		<div class="container">
			
				<div class="twelve columns">
					<?php the_sub_field('latest_projects_header_text'); ?>
				</div>
			
			
				<?php echo do_shortcode('[latest_projects no="'.$proj_ct.'" /]'); ?>	
				<?php wp_reset_query(); ?>
			
			<div class="center"><a href="<?php the_sub_field('more_projects_button_link'); ?>" class="button"><?php the_sub_field('more_projects_button_label'); ?></a></div>
			
		</div>
	</section>
		
<?php endif;
	
	
	// Opt In Section
        if( get_row_layout() == 'opt_in_section' ): ?>
	
	<section class="optin-section">
		<div class="container">
			
				<div class="twelve columns">
					<?php the_sub_field('opt_in_header_text'); ?>
					<?php the_field('form_code','options'); ?>
				</div>
				
			
		</div>
		
		<script>jQuery(document).ready(function($){ $("#mc-embedded-subscribe").prop('value', '<?php the_sub_field('modify_opt_in_button_label'); ?>'); });</script>
	</section>
		
<?php endif;
	
	
	// Logo Section
        if( get_row_layout() == 'logo_section' ): 
	//var
	$bg = get_sub_field('section_background_color'); ?>
	
	<section class="logo-section <?php echo $bg; ?>">
		<div class="container">
			
				<div class="twelve columns">
					<?php the_sub_field('logo_header_text'); ?>
				</div>
				
				<?php // check if the nested repeater field has rows of data
        	if( have_rows('logos') ):

			 	echo '<ul>';

			 	// loop through the rows of data
			    while ( have_rows('logos') ) : the_row();

					$logo_img = get_sub_field('logo');
					$logo_lnk = get_sub_field('logo_url'); 

					echo '<li>'; ?>
			
			<?php if( $logo_lnk ): ?>
				<a href="<?php echo $logo_lnk; ?>">
			<?php endif; ?>
						
				<?php echo '<img src="' . $logo_img['url'] . '" alt="' . $logo_img['alt'] . '" />'; ?>
					
					
					<?php if( $logo_lnk ): ?>
				</a>
			<?php endif; ?>
					
				<?php	echo'</li>';

				endwhile;

				echo '</ul>';

			endif; ?>
			
		</div>
		
	</section>
		
<?php endif;
	
		
	
	// Instagram Section
        if( get_row_layout() == 'instagram_section' ): 
	//var
	$bg = get_sub_field('section_background_color'); 
	$ig = get_sub_field('instagram_short_code'); 
	?>
	
	<section class="instagram-section <?php echo $bg; ?>">
		<div class="container">
			
				<div class="twelve columns">
					<?php the_sub_field('instagram_header_text'); ?>
					<?php echo do_shortcode(''.$ig.''); ?>
					<span class="ig-handle"><a href="https://www.instagram.com/<?php the_sub_field('instagram_handle'); ?>" target="_blank">@<?php the_sub_field('instagram_handle'); ?></a></span>
				</div>
				
			
		</div>
	</section>
		
<?php endif;


// WYSIWYG Section
        if( get_row_layout() == 'wysiwyg_section' ): 
	//var
	$bg = get_sub_field('section_background_color'); 
	$content = get_sub_field('wysiwyg_editor'); 
	?>
	
	<section class="wysiwyg-section <?php echo $bg; ?>">
		<div class="container">
			<div class="row">
				<div class="twelve columns">	
					<?php echo $content; ?>		
				</div>
			</div>
		</div>
	</section>
		
<?php endif;



// Testimonials Section
        if( get_row_layout() == 'testimonials_section' ): 
	//var
	$bg_img = get_sub_field('background_image'); 
	$bg_color = get_sub_field('background_color'); ?>
	
	<?php if( !empty($bg_img) ): ?>
	<section class="testimonials-section" style="background-image:linear-gradient(rgba(0, 0, 0, 0.10), rgba(0, 0, 0, 0.10)),url(<?php echo $bg_img['url']; ?>);">
	<?php else: ?>
	<section class="testimonials-section" style="background-color:<?php echo $bg_color ?>;">
	<?php endif; ?>
	
		<div class="container">
				
				<?php // check if the nested repeater field has rows of data
        	if( have_rows('testimonials') ):

			 	echo '<div class="testimonials-container">';

			 	// loop through the rows of data
			    while ( have_rows('testimonials') ) : the_row();

					//var
			 		$auth_name = get_sub_field('author_name');
			 		$auth_title = get_sub_field('author_title');
			 		$auth_quote = get_sub_field('author_quote');
					$attachment_id = get_sub_field('author_image');
					$size = "author-image"; // (thumbnail, medium, large, full or custom size)
    				$auth_img = wp_get_attachment_image_src( $attachment_id, $size );
					// url = $image[0];
					// width = $image[1];
					// height = $image[2]; 

					echo '<div class="testimonial-quote">'; ?>
			
						
			<div class="author-image-meta">
				<?php echo '<img src="' . $auth_img[0] . '" alt="' . $logo_img['alt'] . '" />'; ?>
				<span class="auth-name"><?php echo $auth_name; ?></span>
				<span class="auth-title"><?php echo $auth_title; ?></span>
			</div>
				
			<div class="author-quote-text"><?php echo $auth_quote; ?></div>			
					
					
				<?php	echo'</div>';

				endwhile;

				echo '</div>';

			endif; ?>
			
		</div>
		
	</section>
		
<?php endif;
		
// Columns Section		
		if ( get_row_layout() == 'columns_section' ) : ?>
			<?php if ( have_rows( 'columns' ) ): ?>
			<div class="container">
				<?php while ( have_rows( 'columns' ) ) : the_row(); ?>
					<?php if ( get_row_layout() == 'full_width' ) : ?>
					<div class="full-width">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'one_half' ) : ?>
					<div class="one-half">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="one-half last">	
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'one_third' ) : ?>
					<div class="one-third">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="one-third">	
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="one-third last">	
						<?php the_sub_field( 'column_3' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'one_fourth' ) : ?>
					<div class="one-fourth">	
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="one-fourth">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="one-fourth">
						<?php the_sub_field( 'column_3' ); ?>
					</div>
					<div class="one-fourth last">
						<?php the_sub_field( 'column_4' ); ?>	
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'one_fifth' ) : ?>
					<div class="one-fifth">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="one-fifth">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="one-fifth">
						<?php the_sub_field( 'column_3' ); ?>
					</div>
					<div class="one-fifth">
						<?php the_sub_field( 'column_4' ); ?>
					</div>
					<div class="one-fifth last">
						<?php the_sub_field( 'column_5' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'one_sixth' ) : ?>
					<div class="one-sixth">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
						<div class="one-sixth">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="one-sixth">
						<?php the_sub_field( 'column_3' ); ?>
					</div>
					<div class="one-sixth">
						<?php the_sub_field( 'column_4' ); ?>
					</div>
					<div class="one-sixth">
						<?php the_sub_field( 'column_5' ); ?>
					</div>
					<div class="one-sixth last">
						<?php the_sub_field( 'column_6' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'two_third_one_third' ) : ?>
					<div class="two-third">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="one-third last">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'one_third_two_third' ) : ?>
					<div class="one-third">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="two-third last">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'one_fourth_three_fourth' ) : ?>
					<div class="one-fourth">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="three-fourth last">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'three_fourth_one_fourth' ) : ?>
					<div class="three-fourth">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="one-fourth last">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'two_fifth_three_fifth' ) : ?>
					<div class="two-fifth">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="three-fifth last">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php elseif ( get_row_layout() == 'three_fifth_two_fifth' ) : ?>
					<div class="three-fifth">
						<?php the_sub_field( 'column_1' ); ?>
					</div>
					<div class="two-fifth last">
						<?php the_sub_field( 'column_2' ); ?>
					</div>
					<div class="clear-fix"></div>
					<?php endif; ?>
					<?php endwhile; ?>
			<?php else: ?>
				<?php // no layouts found ?>
			<?php endif; ?>
	</div>
	<?php	endif;
		
    endwhile;

else :

    // no layouts found

endif;

?>
	

<?php get_footer(); ?>