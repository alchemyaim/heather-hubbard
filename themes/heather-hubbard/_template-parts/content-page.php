<?php
/**
 * Template part for displaying page content in page.php.
 *
 */
?>

<article>
 <?php //Hide Title
	if(get_field('hide_title') == "true"){?>
	<style>.entry-header{display:none;}</style>
<?php } ?>
 
  <header class="entry-header">
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  </header>
  <!-- .entry-header -->
  
  <div class="entry-content">
    <?php the_content(); ?>
  </div>
  <!-- /.entry-content -->
  
</article>
<!-- /#post-## --> 