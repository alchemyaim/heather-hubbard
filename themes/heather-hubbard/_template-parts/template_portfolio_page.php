<?php
/*---------------------------------------------------------
	Template Name: Portfolio Page
------------------------------------------------------------*/
	get_header();
?>

	<div id="portfolio" class="container">

		<ul class="filter">
			<li><a class="active" href="#" data-filter="*">All</a></li>
			<?php 
			$portfolio_categories = get_categories(array('taxonomy'=>'portfolio_category'));
			foreach($portfolio_categories as $portfolio_category)
				echo '<li><a href="#" data-filter=".' . $portfolio_category->slug . '">' . $portfolio_category->name . '</a></li>';
			?>
		</ul>

		<ul class="items">
			<div class="gutter-sizer"></div>
			<?php 

				$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				$args = array( 'posts_per_page' => -1, 
					   'offset'=> 0,
					   'post_type' => 'portfolio');

				$all_posts = new WP_Query($args);

				while($all_posts->have_posts()) : $all_posts->the_post();

			?>

			<li class="item <?php $terms = get_the_terms($post->ID, 'portfolio_category' );
						if ($terms && ! is_wp_error($terms)) :
    					$tslugs_arr = array();
    					foreach ($terms as $term) {
						$tslugs_arr[] = $term->slug;
    					}
    					$terms_slug_str = join( " ", $tslugs_arr);
						endif;
						echo $terms_slug_str; ?>">
				<a href="<?php the_permalink(); ?>">
					<div class="caption">
						<h3><?php the_title(); ?></h3>
						<span class="short-desc"><?php the_field('project_short_description'); ?></span>
						
						
						
						<span class="icon <?php the_field('type_of_media') ?>"></span>
						

					</div>
					<?php the_post_thumbnail('portfolio-image'); ?>
				</a>
			</li>

			<?php endwhile; ?>
			

		</ul>

	</div>
    

<?php get_footer(); ?>