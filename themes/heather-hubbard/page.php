<?php
/**
 * Default Page Template
 */

get_header();
?>

<?php //White Nav
	if(get_field('white_nav') == "true"){?>
		<style>#linkbar a{color:#fff;}#slideout-bar,#slideout-bar:before,#slideout-bar:after{background-color: #fff;</style>
<?php } ?>

<?php //Hero Header
	if(get_field('header') == "true" && get_field('header_type') == "hero") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$bg_hero_img = get_field('header_image');
	$bg_hero_text = get_field('header_text');	
?>

<!-- START HERO HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<section class="hero-header" style="background-image:linear-gradient(rgba(0,0,0, 0.10), rgba(0,0,0, 0.10)),url(<?php echo $bg_hero_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END HERO HEADER -->
<?php } ?>


<?php //Hero Slider
if(get_field('header') == "true" && get_field('header_type') == "slider") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
?>

<!-- START SLIDER HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<?php // check if the repeater field has rows of data
if( have_rows('header_slider') ): ?>
<section id="hero-slider">
<?php	// loop through the rows of data
    while ( have_rows('header_slider') ) : the_row(); 

	//var
	$bg_slider_img = get_sub_field('slider_background_image');
	$bg_slider_text = get_sub_field('slider_text');	
?>

<div class="slider-header slide" style="background-image:linear-gradient(rgba(0,0,0, 0.10), rgba(0,0,0, 0.10)),url(<?php echo $bg_slider_img['url']; ?>);min-height:<?php echo $hero_ht; ?>">

	<div class="container">
		<div class="slider-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_sub_field('text_position_and_width') ?>">
			<div class="slider-text"><?php echo $bg_slider_text; ?></div>
		</div>
	</div>
</div>

<?php endwhile; ?>
</section>
<?php else :
    // no rows found
endif; ?>

<!-- END SLIDER HEADER -->
<?php } ?>


<?php //Hero Video Header
	if(get_field('header') == "true" && get_field('header_type') == "video") { 
	
	//var
	$hero_ht = get_field('hero_header_height');
	$vid_hero_poster = get_field('header_poster');
	$vid_hero_video = get_field('header_video');
	$vid_hero_video_url = get_field('header_video_external_url');
	$vid_hero_loop = get_field('header_video_loop');
	$vid_hero_mute = get_field('header_video_mute');
	$bg_hero_text = get_field('header_text');
		
		if(get_field('header_video_loop') == 1){ 
			$vid_loop = "true";
		}
		if(get_field('header_video_loop') == 0){
			$vid_loop = "false";
		}
		if(get_field('header_video_mute') == 1){ 
			$vid_mute = "true";
		}
		if(get_field('header_video_mute') == 0){
			$vid_mute = "false";
		}
?>

<!-- START VIDEO HEADER -->
<style>#header-wrapper{background-color:transparent;}#header-wrapper{margin-bottom:0;border-bottom:1px solid #ccc;}#content-wrapper{margin-top:0}</style>

<section class="hero-header-vid" data-vide-bg="mp4: <?php if( !empty($vid_hero_video) ){ echo $vid_hero_video;} elseif( !empty($vid_hero_video_url) ){echo $vid_hero_video_url;} ?>, poster: <?php echo $vid_hero_poster['url']; ?>" data-vide-options="posterType: png, loop: <?php echo $vid_loop; ?>, muted: <?php echo $vid_mute; ?>, position: 50% 50%">
	<div class="container">
		<div class="hero-text-outer" style="height:<?php echo $hero_ht; ?>;<?php echo the_field('header_text_width_position_hero') ?>">
			<div class="hero-text"><?php echo $bg_hero_text; ?></div>
		</div>
	</div>
</section>
<!-- END VIDEO HEADER -->
<?php } ?>


<div class="page-content container">

    <?php
    while ( have_posts() ) : the_post();

        get_template_part( '_template-parts/content', 'page' );

    endwhile; // End of the loop.
    ?>

</div> <!-- /.container -->

<?php get_footer(); ?>
