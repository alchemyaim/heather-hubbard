<?php
/*---------------------------------
	LOAD THEME FRAMEWORK
------------------------------------*/
// Load Extra Components
require_once( get_template_directory() . '/_includes/functions_extra.php' );

/*****************************************************************************************

	Below are the most important functions of the theme. Edit carefully! :)

*****************************************************************************************/

/*---------------------------------------
	Clean up the WordPress header output
-----------------------------------------*/

	function removeHeadLinks() {
	remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
	remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
	remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
	remove_action( 'wp_head', 'index_rel_link' ); // index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
	remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	}
	add_action('init', 'removeHeadLinks');
	remove_action('wp_head', 'wp_generator');

/*----------------------------------------
	Make some adjustments on theme setup
------------------------------------------*/

if ( ! function_exists( 'theme_setup' ) ):
	function theme_setup() {

		//Define content width
		if(!isset($content_width)) $content_width = 940;

		//Style the visual editor with editor-style.css to match the theme style.
		add_editor_style(get_template_directory_uri() . '/_static/styles/admin/editor-style.css');

		//Add post thumbnails
		add_theme_support( 'post-thumbnails' );

		//Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );

		$locale = get_locale();
		$locale_file = get_template_directory() . "/lang/$locale.php";

		if ( is_readable( $locale_file ) )
			require_once( $locale_file );

		//This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
		'primary' => __( 'Main Navigation', 'alchemyaim' )
		) );

		//Define custom thumbnails
		set_post_thumbnail_size( 151, 110, true );
		add_image_size( 'thumb-gallery',  312, 250, true );
	}
endif;
add_action( 'after_setup_theme', 'theme_setup' );

/*---------------------------------
	Make some changes to the wp_title() function
------------------------------------*/

function theme_filter_wp_title( $title, $separator ) {

	if ( is_feed() ) return $title;

	global $paged, $page;

	if ( is_search() ) {

		//If we're a search, let's start over:
		$title = sprintf( __( 'Search results for %s', 'theme' ), '"' . get_search_query() . '"' );
		//Add a page number if we're on page 2 or more:
		if ( $paged >= 2 )
			$title .= " $separator " . sprintf( __( 'Page %s', 'theme' ), $paged );
		//Add the site name to the end:
		$title .= " $separator " . get_bloginfo( 'name', 'display' );
		//We're done. Let's send the new title back to wp_title():
		return $title;
	}

	//Otherwise, let's start by adding the site name to the end:
	$title .= get_bloginfo( 'name', 'display' );

	//If we have a site description and we're on the home/front page, add the description:
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title .= " $separator " . $site_description;

	//Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		$title .= " $separator " . sprintf( __( 'Page %s', 'theme' ), max( $paged, $page ) );

	//Return the new title to wp_title():
	return $title;
}
add_filter( 'wp_title', 'theme_filter_wp_title', 10, 2 );

/*---------------------------------
	Create a wp_nav_menu() fallback, to show a home link
------------------------------------*/

function theme_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'theme_page_menu_args' );

/*---------------------------------
	Comments Callback Function
------------------------------------*/

function commentlist($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment ?>
	<li <?php comment_class('comment'); ?> id="comment-<?php comment_ID() ?>"><div id="div-comment-<?php comment_ID(); ?>" class="comments">
		<p class="comments-block">
        <div class="user"><?php echo get_avatar( $comment, 96 ); ?></div>
        <div class="message">
        <div class="reply-link"><?php comment_reply_link(array_merge( $args, array('add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?></div>
        <div class="info">
			<div class="comment-author"><?php comment_author_link() ?> </div>
			<div class="comment-time comment-meta commentmetadata"><a class="date" href="#comment-<?php comment_ID(); ?>" title="Permanent Link to this comment"><?php comment_date('F j, Y'); echo " AT "; ?><?php comment_time('g:iA'); ?></a> <?php edit_comment_link('Edit','  (',')'); ?></div>
		</p>
		<div class="comment-body">
        
			<?php comment_text() ?>
			<?php if ($comment->comment_approved == '0') : ?>
				<p>Thank you. Comments are moderated. Your comment will appear shortly.</p>
			<?php endif; ?>
		</div>
        </div>
        </div>
		
	</div>
<?php }
// end comment callback function (note: no need to close with </li>)

/*---------------------------------
	Custom Local Avatars
------------------------------------*/

add_filter('get_avatar', 'tsm_acf_profile_avatar', 10, 5);
function tsm_acf_profile_avatar( $avatar, $id_or_email, $size, $default, $alt ) {

    // Get user by id or email
    if ( is_numeric( $id_or_email ) ) {

        $id   = (int) $id_or_email;
        $user = get_user_by( 'id' , $id );

    } elseif ( is_object( $id_or_email ) ) {

        if ( ! empty( $id_or_email->user_id ) ) {
            $id   = (int) $id_or_email->user_id;
            $user = get_user_by( 'id' , $id );
        }

    } else {
        $user = get_user_by( 'email', $id_or_email );
    }

    // Get the user id
    $user_id = $user->ID;

    // Get the file id
    $image_id = get_user_meta($user_id, 'tsm_local_avatar', true); // CHANGE TO YOUR FIELD NAME

    // Bail if we don't have a local avatar
    if ( ! $image_id ) {
        return $avatar;
    }

    // Get the file size
    $image_url  = wp_get_attachment_image_src( $image_id, 'bio-pic' ); // Set image size by name
    // Get the file url
    $avatar_url = $image_url[0];
    // Get the img markup
    $avatar = '<img alt="' . $alt . '" src="' . $avatar_url . '" class="avatar avatar-' . $size . '" height="' . $size . '" width="' . $size . '"/>';

    // Return our new avatar
    return $avatar;
}

/*---------------------------------
	Feature to RSS
------------------------------------*/

function featuredtoRSS($content) {global $post;
if ( has_post_thumbnail( $post->ID ) ){
$content = '' . get_the_post_thumbnail( $post->ID, 'full', array( 'style' => 'margin:10px auto;display:block;' ) ) . '' . $content;
}
return $content;
}

add_filter('the_excerpt_rss', 'featuredtoRSS');
add_filter('the_content_feed', 'featuredtoRSS');

/*---------------------------------
	Add Featured Image Column for Posts
------------------------------------*/

add_filter('manage_posts_columns', 'add_thumbnail_column', 5);

function add_thumbnail_column($columns){
  $columns['new_post_thumb'] = __('Featured Image');
  return $columns;
}

add_action('manage_posts_custom_column', 'display_thumbnail_column', 5, 2);

function display_thumbnail_column($column_name, $post_id){
  switch($column_name){
    case 'new_post_thumb':
      $post_thumbnail_id = get_post_thumbnail_id($post_id);
      if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail' );
        echo '<img width="100" src="' . $post_thumbnail_img[0] . '" />';
      }
      break;
  }
}

/*---------------------------------
	Add Featured Image Column for Pages
------------------------------------*/

add_filter( 'manage_pages_columns', 'custom_pages_columns' );
function custom_pages_columns( $columns ) {

  $columns['new_page_thumb'] = __('Featured Image');
  return $columns;
}

add_action( 'manage_pages_custom_column', 'custom_page_column_content', 10, 2 );

function custom_page_column_content( $column_name, $post_id ) {
  switch($column_name){
    case 'new_page_thumb':
    $post_thumbnail_id = get_post_thumbnail_id($post_id);
    if ($post_thumbnail_id) {
      $post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'thumbnail' );
      echo '<img width="100" src="' . $post_thumbnail_img[0] . '" />';
    }
     break;
  }
}

/* ------------------------
-----   Show Page/Post IDs    -----
------------------------------*/

/*Forked from "Simply Show IDs" Plugin*/
// Prepend the new column to the columns array
function ssid_column($cols) {
	$cols['ssid'] = 'ID';
	return $cols;
}

// Echo the ID for the new column
function ssid_value($column_name, $id) {
	if ($column_name == 'ssid')
		echo $id;
}

function ssid_return_value($value, $column_name, $id) {
	if ($column_name == 'ssid')
		$value = $id;
	return $value;
}

// Output CSS for width of new column
function ssid_css() {
?>
<style type="text/css">
	#ssid { width: 50px; } /* Simply Show IDs */
</style>
<?php
}

// Actions/Filters for various tables and the css output
function ssid_add() {
	add_action('admin_head', 'ssid_css');

	add_filter('manage_posts_columns', 'ssid_column');
	add_action('manage_posts_custom_column', 'ssid_value', 10, 2);

	add_filter('manage_pages_columns', 'ssid_column');
	add_action('manage_pages_custom_column', 'ssid_value', 10, 2);

	add_filter('manage_media_columns', 'ssid_column');
	add_action('manage_media_custom_column', 'ssid_value', 10, 2);

	add_filter('manage_link-manager_columns', 'ssid_column');
	add_action('manage_link_custom_column', 'ssid_value', 10, 2);

	add_action('manage_edit-link-categories_columns', 'ssid_column');
	add_filter('manage_link_categories_custom_column', 'ssid_return_value', 10, 3);

	foreach ( get_taxonomies() as $taxonomy ) {
		add_action("manage_edit-${taxonomy}_columns", 'ssid_column');
		add_filter("manage_${taxonomy}_custom_column", 'ssid_return_value', 10, 3);
	}

	add_action('manage_users_columns', 'ssid_column');
	add_filter('manage_users_custom_column', 'ssid_return_value', 10, 3);

	add_action('manage_edit-comments_columns', 'ssid_column');
	add_action('manage_comments_custom_column', 'ssid_value', 10, 2);
}

add_action('admin_init', 'ssid_add');


/*---------------------------------
	Function that replaces the default the_excerpt() function
------------------------------------*/

function new_excerpt_length($length) {
    return 50;
}
add_filter('excerpt_length', 'new_excerpt_length');

function new_excerpt_more( $more ) {
	return ' ...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


function excerpt($length_callback='', $more_callback='') {

    global $post;

    if(function_exists($length_callback)){
		add_filter('excerpt_length', $length_callback);
    }

    if(function_exists($more_callback)){
		add_filter('excerpt_more', $more_callback);
    }

    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = $output;

    echo $output;

}


/*---------------------------------------------------------------
	A custom excerpt function (Usage: <?php echo new_excerpt(25); ?>
----------------------------------------------------------------*/		
function new_excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);
      if (count($excerpt)>=$limit) {
        array_pop($excerpt);
        $excerpt = implode(" ",$excerpt).'...';
      } else {
        $excerpt = implode(" ",$excerpt);
      } 
      $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
      return $excerpt;
    }

    function content($limit) {
      $content = explode(' ', get_the_content(), $limit);
      if (count($content)>=$limit) {
        array_pop($content);
        $content = implode(" ",$content).'...';
      } else {
        $content = implode(" ",$content);
      } 
      $content = preg_replace('/\[.+\]/','', $content);
      $content = apply_filters('the_content', $content); 
      $content = str_replace(']]>', ']]&gt;', $content);
      return $content;
    }
	
	
	function modify_read_more_link() {
    return '<a class="more-link" href="' . get_permalink() . '"> Continue reading...</a>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );

/*---------------------------------
	A custom pagination class
------------------------------------*/

add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes() {
    return 'class="button"';
}


add_filter('next_post_link', 'post_link_attributes');
add_filter('previous_post_link', 'post_link_attributes');

function post_link_attributes($output) {
    $injection = 'class="button"';
    return str_replace('<a href=', '<a '.$injection.' href=', $output);
}

/*---------------------------------
	Count post views
------------------------------------*/

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return 0;
    }
    return $count;
}

function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

/*---------------------------------
	Deal with WP empty paragraphs
------------------------------------*/

function formatter($content) {

	$bad_content = array('<p></div></p>', '<p><div class="full', '_width"></p>', '</div></p>', '<p><ul', '</ul></p>', '<p><div', '<p><block', 'quote></p>', '<p><hr /></p>', '<p><table>', '<td></p>', '<p></td>', '</table></p>', '<p></div>', 'nosidebar"></p>', '<p><p>', '<p><a', '</a></p>', '-half"></p>', '-third"></p>', '-fourth"></p>', '<p><p', '</p></p>', 'child"></p>', '<p></p>', '-fifth"></p>', '-sixth"></p>', 'last"></p>', 'fix" /></p>', '<p><hr', '<p><li', '"centered"></p>', '</li></p>', '<div></p>', '<p></ul>', '<p><img', ' /></p>', '"nop"></p>', 'tures"></p>', '"left"></p>', '<p><h1 class="center">', 'centered"></p>');
	$good_content = array('</div>', '<div class="full', '_width">', '</div>', '<ul', '</ul>', '<div', '<block', 'quote>', '<hr />', '<table>', '<td>', '</td>', '</table>', '</div>', 'nosidebar">', '<p>', '<a', '</a>', '-half">', '-third">', '-fourth">', '<p', '</p>', 'child">', '', '-fifth">', '-sixth">', 'last">', 'fix" />', '<hr', '<li', '"centered">', '</li>', '<div>', '</ul>', '<img', ' />', '"nop">', 'tures">', '"left">', '<h1 class="center">', 'centered">');

	$new_content = str_replace($bad_content, $good_content, $content);
	return $new_content;

}

remove_filter('the_content', 'wpautop');
add_filter('the_content', 'wpautop', 10);
add_filter('the_content', 'formatter', 11);

/*---------------------------------
	Register Menu Areas
------------------------------------*/
register_nav_menus( array(  
'primary' => __( 'Main Navigation', 'alchemyaim' ),
'mobile' => __( 'Mobile Navigation', 'alchemyaim' ),
) );

/*---------------------------------
	Fix empty search issue
------------------------------------*/

function request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}
add_filter('request', 'request_filter');

/*---------------------------------
	Enqueue custom admin scripts & styles
------------------------------------*/

function add_admin_stuff(){
	wp_register_style('custom_admin_styles', get_template_directory_uri(). '/_static/styles/admin/admin_styles.css');
	wp_enqueue_style('custom_admin_styles');
}
add_action( 'admin_init', 'add_admin_stuff' );


/*---------------------------------
	Enqueue custom admin scripts & styles
------------------------------------*/

function theme_scripts() {
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/_static/js/plugins.js', array(), '1.0.0', true );
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/_static/js/scripts.js', array(), '1.0.0', true );
	wp_enqueue_script( 'acf_seo', get_template_directory_uri() . '/_static/js/acf_yoastseo.js', array(), '1.0.0', true );
	}

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

/*---------------------------------
	Register widget areas
------------------------------------*/
add_filter( 'widget_text', 'do_shortcode'); /* Enable Shortcodes on Widgets */

function grow_widgets_init() {
	register_sidebar( array(
		'name' => __('Sidebar', 'theme'),
		'id' => 'sidbar_widget',
		'description' => __('The sidebar of the theme.', 'theme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	
    register_sidebar( array(
        'name'          => __( 'Nav Menu Widget Area', 'grow' ),
        'id'            => 'nav-widget',
        'description'   => __( 'A widget area for the nav bar.', 'grow' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ) );
 
    register_sidebar( array(
        'name'          => __( 'Footer First Widget Area', 'grow' ),
        'id'            => 'footer-widget-1',
        'description'   => __( 'The top footer\'s first widget area.', 'grow' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
    ) );
	
	register_sidebar( array(
        'name'          => __( 'Footer Second Widget Area', 'grow' ),
        'id'            => 'footer-widget-2',
        'description'   => __( 'The top footer\'s second widget area.', 'grow' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
	
	register_sidebar( array(
        'name'          => __( 'Footer Third Widget Area', 'grow' ),
        'id'            => 'footer-widget-3',
        'description'   => __( 'The top footer\'s third widget area.', 'grow' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
	
	register_sidebar( array(
        'name'          => __( 'Footer Fourth Widget Area', 'grow' ),
        'id'            => 'footer-widget-4',
        'description'   => __( 'The top footer\'s fourth widget area.', 'grow' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
	
	register_sidebar( array(
		'name' 			=> __('Footer\'s bottom first widget area'),
		'id' 			=> 'footer-widget-5',
		'description' 	=> __('The footer\'s bottom stripe first widget area(1/2)'),
		'before_widget' => '<div id="%1$s" class="widget left %2$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<span class="hidden">',
		'after_title' 	=> '</span>',
	) );

	/*register_sidebar( array(
		'name' 			=> __('Footer\'s bottom second widget area'),
		'id' 			=> 'footer-widget-6',
		'description' 	=> __('The footer\'s bottom stripe second widget area(2/2)'),
		'before_widget' 	=> '<div id="%1$s" class="widget right %2$s">',
		'after_widget' 	=> '</div>',
		'before_title' 	=> '<span class="hidden">',
		'after_title' 	=> '</span>',
	) );*/
}
add_action( 'widgets_init', 'grow_widgets_init' );

/*---------------------------------
	Thumbnail Size Support
------------------------------------*/
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 150, 150, true ); // Normal post thumbnails
add_image_size( 'featured-thumbnail', 60, 60 ); // Featured thumbnail size
add_image_size( 'related-posts', 250, 130, true ); // Related Posts thumbnail size
add_image_size( 'pop-posts-image', 75, 75, true ); // Popular Posts thumbnail size
add_image_size( 'portfolio-image', 380, 300, true ); // Popular Posts thumbnail size
add_image_size( 'recent-posts-image', 575, 307, true ); // Recent Posts thumbnail size
add_image_size( 'team-image', 275, 315, true ); // Team thumbnail size
add_image_size( 'author-image', 100, 100, true ); // Testimonial Author thumbnail size
add_image_size( 'bio-pic', 400, 400, true ); // Testimonial Author thumbnail size

/*------------------------------------------------
	Add Support for Facebook Feature Image Share
---------------------------------------------------*/
function insert_image_src_rel_in_head() {
	global $post;
	if ( !is_singular()) //if it is not a post or a page
		return;
	if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
		$default_image = get_template_directory() . '/_static/images/default-image.jpg'; //replace this with a default image on your server or an image in your media library
		echo '<meta property="og:image" content="' . $default_image . '"/>';
	}
	else{
		$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
		echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
	}
	echo "
";
}
add_action( 'wp_head', 'insert_image_src_rel_in_head', 5 );

/*------------------------------------------------
	Page Slug Body Class
---------------------------------------------------*/
function add_slug_body_class( $classes ) {
global $post;
if ( isset( $post ) ) {
$classes[] = $post->post_type . '-' . $post->post_name;
}
return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


/*------------------------------------------------
	WooCommerce Filters
---------------------------------------------------*/
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

//Woocommerce 3.0 Gallery support
add_action( 'after_setup_theme', 'grow_setup' );

function grow_setup() {
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}

// Display 8 products per page. Goes in functions.php
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 8;' ), 20 );

add_filter( 'woocommerce_show_page_title' , 'woo_hide_page_title' );

// Removes the "shop" title on the main shop page
function woo_hide_page_title() {	
	return false;
}

// Removes the "Reviews" tab on the single product page
add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
    function wcs_woo_remove_reviews_tab($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}

// Move product tabs
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 60 );

/*------------------------------------------------
	Gravity Forms Hide Labels Filter
---------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );
add_filter( 'option_use_smilies', '__return_false' );


/*------------------------------------------------
	Author Social Links
---------------------------------------------------*/
function filtering_defualt_contacts ( $contact, $user ) {

    // first we suppress the legacy fields but we check that are empty in the user profile
    foreach ( array ( 'aim', 'yim', 'jabber' ) as $method ) {
        // we check if the current user has data in this old fields
	if ( isset ( $user->$method ) && ( trim( $user->$method ) ) ) continue;
        unset( $contact[ $method ] );
    }

    return $contact;

}
add_filter( 'user_contactmethods', 'filtering_defualt_contacts', 99, 2 );

function my_new_contactmethods( $contactmethods ) {
// Add Google Plus
$contactmethods['googleplus'] = 'Google+';
// Add Instagram
$contactmethods['instagram'] = 'Instagram';
// Add Pinterest
$contactmethods['pinterest'] = 'Pinterest';
// Add Twitter
$contactmethods['twitter'] = 'Twitter';
//add Facebook
$contactmethods['facebook'] = 'Facebook';
return $contactmethods;
}
add_filter('user_contactmethods','my_new_contactmethods',10,1);

?>