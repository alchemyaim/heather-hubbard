/* Add theme option styles here */
/* FONTS */
.comments-ct,
.tags,
#comments .message .reply-link,
.aside .widget.rich_text a span,
.cd-primary-nav a,
.cd-primary-nav ul a,
.cd-primary-nav ul .button,
.woocommerce span.onsale,
.woocommerce nav.woocommerce-pagination ul li a,
.page-numbers.current,
.woocommerce .quantity input[type="number"],
.woocommerce div.product .woocommerce-tabs ul.tabs li a,
.woocommerce-variation-description p,
#slideout-menu a,
.toggle-title,
.project-details,
h1,
h2,
h3,
h4,
h5,
h6,
label,
legend,
.share-text,
.comment-author,
#linkbar a,
.aside .widget.social .social-title,
.widget.posts ul.posts-list li a,
.woocommerce .quantity:before,
.toggle-title,
.accordion > section .accordion-title,
.tab a,
ul.filter li,
.cart .product-name,
.cart .product-price,
.cart .product-quantity,
.cart .product-subtotal,
.cart-subtotal,
.tax-rate,
.order-total,
.team-name,
.auth-name,
.author-quote-text,
#top-footer .widget.social .social-title,
.blog-intro{
    font-family: <?php the_field('heading_font','option');?>;
}
 
.tags a, body, #credits, .button,
button,
input[type="submit"],
input[type="reset"],
input[type="button"] {
    font-family: <?php the_field('body_font','option');?>;
}



/* COLORS */
a,
.woocommerce-variation-description p,
.toggle-title:hover,
.toggle-title.active,
.accordion > section .accordion-title:hover,
.accordion > section .accordion-title:hover,
.accordion > section .accordion-title:hover:before,
.accordion > section.opened .accordion-title,
.accordion > section.opened .accordion-title:before,
.tab a:hover,
.tab.active a,
ul.filter li a:hover,
ul.filter li a.active,
a:hover,
#linkbar a:hover, #header-wrapper.smaller #linkbar a:hover,
#header-wrapper.smaller #linkbar .current_page_item a,
#credits .social-links a:hover,
#linkbar .current-menu-item a,
#linkbar .current_page_item a,
.widget.posts ul.posts-list li a:hover,
.button:hover,
button:hover,
input[type="submit"]:hover,
input[type="reset"]:hover,
input[type="button"]:hover,
.button:focus,
button:focus,
input[type="submit"]:focus,
input[type="reset"]:focus,
input[type="button"]:focus,
.latest-blog-wrapper h6 a:hover,
.ig-handle a:hover,
 .author_text a:hover{
    color: <?php the_field('accent_color_1','option');?>;
}

.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce div.product .woocommerce-tabs ul.tabs li a:hover,
.woocommerce-message:before,
.woocommerce-info:before,
#top-footer .widget.social .social-links a{
	color: <?php the_field('accent_color_1','option');?> !important;
}

h1,
h2,
h3,
h4,
h5,
h6, 
.latest-blog-wrapper h6 a, 
.project-desc h2, 
.post-title a, 
.aside .widget.social .social-title, 
h1.product_title, 
.woocommerce-Tabs-panel h2{
	color: <?php the_field('heading_colors','option');?>;
}

.single-product .woocommerce-Price-amount{
	color: <?php the_field('heading_colors','option');?> !important;
}

label,
legend,
.share-text,
.comment-author,
#linkbar a,
.widget.posts ul.posts-list li a,
.woocommerce .quantity:before,
.toggle-title,
.accordion > section .accordion-title,
.tab a,
ul.filter li,
.aside .widget.social a:hover,
#credits .social-links a,
.woocommerce ul.products li.product .price ins,
ul.filter li a,
.share-buttons a:hover,
.team-name,
#header-wrapper.smaller #linkbar a, #linkbar li ul li a, #top-footer .widget .side-bar-form h2.form-title, #top-footer p{
	color: <?php the_field('accent_color_2','option');?>;
}

.woocommerce div.product .woocommerce-tabs ul.tabs li a{
	color: <?php the_field('accent_color_2','option');?> !important;
}

.button.button-primary:hover,
button.button-primary:hover,
input[type="submit"].button-primary:hover,
input[type="reset"].button-primary:hover,
input[type="button"].button-primary:hover,
.button.button-primary:focus,
button.button-primary:focus,
input[type="submit"].button-primary:focus,
input[type="reset"].button-primary:focus,
input[type="button"].button-primary:focus,
.woocommerce ul.products li.product .button:hover,
.back-shop-btn:hover,
.button:hover,
button:hover,
input[type="submit"]:hover,
input[type="reset"]:hover,
input[type="button"]:hover,
.button:focus,
button:focus,
input[type="submit"]:focus,
input[type="reset"]:focus,
input[type="button"]:focus,
#search_form .button,
.woocommerce div.product form.cart .button:hover, .header-optin .optin-section #mc_embed_signup input[type="submit"], .header-optin .optin-section #mc_embed_signup button{
    border-color: <?php the_field('accent_color_1','option');?>;
}

.optin-form-solo input[type="submit"]{
	 border-color: <?php the_field('accent_color_1','option');?> !important;
}

.cd-nav-trigger span,
.cd-nav-trigger span::before,
.cd-nav-trigger span::after,
.cd-nav-trigger.nav-is-visible span::before,
.cd-nav-trigger.nav-is-visible span::after,
#slideout-bar,
#slideout-bar:before,
#slideout-bar:after,
#linkbar.smaller #slideout-bar,#linkbar.smaller #slideout-bar:before, #linkbar.smaller #slideout-bar:after, footer, .optin-section,
.side-bar-form #mc_embed_signup input[type="submit"]:hover, #about_author, .close-lines1, .close-lines2{
    background-color: <?php the_field('accent_color_2','option');?>;
}

.woocommerce ul.products li.product .button,
.woocommerce div.product form.cart .button,
.back-shop-btn,
.woocommerce-message a.button.wc-forward,
.woocommerce input.button,
.checkout-button, #top-footer #mc_embed_signup input[type="submit"], .gform_wrapper .gform_footer input.button:hover, .gform_wrapper .gform_footer input[type=submit]:hover, #top-footer #mc_embed_signup button{
    background-color: <?php the_field('accent_color_2','option');?> !important;
	opacity:1 !important;
}
.woocommerce ul.products li.product .button, .optin-form-solo input[type="submit"]:hover, #top-footer #mc_embed_signup input[type="submit"], .gform_wrapper .gform_footer input.button:hover, .gform_wrapper .gform_footer input[type=submit]:hover, #top-footer #mc_embed_signup button{
	border-color: <?php the_field('accent_color_2','option');?> !important;
}

.button.button-primary,
button.button-primary,
input[type="submit"].button-primary,
input[type="reset"].button-primary,
input[type="button"].button-primary,
#search_form .button:hover,
#top-footer .button:hover,
.woocommerce div.product form.cart .button, #slideout-menu, #slideout-menu a, #linkbar li ul{
	background-color: <?php the_field('accent_color_4','option');?>;
	color:<?php the_field('accent_color_2','option');?>;
}

.page-about #linkbar .current-menu-item a, .page-about #linkbar .current_page_item a, .page-about #linkbar a:hover, .page-for-attorneys #linkbar .current-menu-item a, .page-for-attorneys #linkbar .current_page_item a, .page-for-attorneys #linkbar a:hover,  #credits a:hover{
	color:<?php the_field('accent_color_4','option');?> !important;
}

.page-about #linkbar li ul li a:hover, .page-for-attorneys #linkbar li ul li a:hover, .page-about #linkbar li lu li.current-menu-item a, .page-about #linkbar li lu li.current_page_item a, .page-for-attorneys #linkbar li ul li.current-menu-item a{
	color:<?php the_field('accent_color_2','option');?> !important;
}

#mc_embed_signup input[type="submit"]:hover, .callout-box .button:hover,#top-footer #mc_embed_signup input[type="submit"]:hover, .nav-older .button:hover, .nav-newer .button:hover, .gform_wrapper .gform_footer input.button, .gform_wrapper .gform_footer input[type=submit], .header-optin .optin-section #mc_embed_signup button:hover, .widget .side-bar-form button:hover, #top-footer #mc_embed_signup button:hover{
	background-color: <?php the_field('accent_color_4','option');?> !important;
	border-color: <?php the_field('accent_color_4','option');?> !important;
	color:<?php the_field('accent_color_2','option');?> !important;
}
.button.button-primary,
button.button-primary,
input[type="submit"].button-primary,
input[type="reset"].button-primary,
input[type="button"].button-primary,
.back-shop-btn,
#search_form .button:hover,
.woocommerce div.product form.cart .button{
	border-color: <?php the_field('accent_color_4','option');?>;
}

#top-footer .button:hover {
	border-color: <?php the_field('accent_color_2','option');?> !important;
}

::-webkit-input-placeholder,
:-moz-placeholder,
::-moz-placeholder,
:-ms-input-placeholder{
    color: <?php the_field('body_font_color','option');?> !important;
}

.comment-time a,
.woocommerce-page .woocommerce-Price-amount.amount, 
body, 
.button,
button,
input[type="submit"],
input[type="reset"],
input[type="button"],
.recent-products .woocommerce ul.products li.product .price,
.recent-products .woocommerce ul.products li.product .price ins,
.ig-handle a,
#mc_embed_signup input[type="text"],#mc_embed_signup input[type="email"],
.team-member a{
    color: <?php the_field('body_font_color','option');?>;
}

blockquote p{
    color: <?php the_field('block_quote_font_color','option');?>;
}


.cd-primary-nav,
.cd-primary-nav > div > ul,
.woocommerce span.onsale,
#search_form .button,
.button.button-primary:hover,
button.button-primary:hover,
input[type="submit"].button-primary:hover,
input[type="reset"].button-primary:hover,
input[type="button"].button-primary:hover,
.button.button-primary:focus,
button.button-primary:focus,
input[type="submit"].button-primary:focus,
input[type="reset"].button-primary:focus,
input[type="button"].button-primary:focus,
.widget .side-bar-form, .optin-form-solo input[type="submit"], .callout-box, #mc_embed_signup input[type="submit"]:hover,
   .optin-section #mc_embed_signup input[type="submit"]:hover,
.gform_wrapper .gform_footer input.button:hover, .gform_wrapper .gform_footer input[type=submit]:hover, .header-optin .optin-section #mc_embed_signup input[type="submit"], .header-optin .optin-section #mc_embed_signup button{
    background-color: <?php the_field('accent_color_1','option');?>;
}

#top-footer, #top-footer .widget .side-bar-form, .page-for-attorneys .about-section .item-content.txt-right, .blog-intro, .page-home .about-section .item-content, .about-middle{
	background-color: <?php the_field('accent_color_3','option');?>;
}

.woocommerce ul.products li.product .button:hover,
.woocommerce div.product form.cart .button:hover,
.back-shop-btn:hover,
.woocommerce-message a.button.wc-forward:hover,
.woocommerce input.button:hover,
.checkout-button:hover,
.recent-products .woocommerce span.onsale, .woocommerce span.onsale{
	background-color: <?php the_field('accent_color_1','option');?> !important;
}

blockquote p {
    border-left: 5px <?php the_field('accent_color_1','option');?> solid;
}

input[type="email"]:focus,
input[type="number"]:focus,
input[type="search"]:focus,
input[type="text"]:focus,
input[type="tel"]:focus,
input[type="url"]:focus,
input[type="password"]:focus,
textarea:focus,
select:focus {
    border: 1px solid <?php the_field('accent_color_1','option');?>  !important;
}

.optin-form-solo input[type="text"],.optin-form-solo input[type="email"]{
	border:1px solid <?php the_field('body_font_color','option');?> !important;
}

.woocommerce-message, .woocommerce-info {
    border-top-color: <?php the_field('accent_color_1','option');?> !important;
}

.button,
button,
input[type="submit"],
input[type="reset"],
input[type="button"] {
    border: 1px solid <?php the_field('body_font_color','option');?>;

}
ul.items li a .caption{
	background-color: <?php the_field('portfolio_overlay_color','option');?>;
}