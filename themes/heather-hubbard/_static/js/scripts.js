/*  -------------------------------------------------------
	Javascript helper document insert any simple JS functions here.
------------------------------------------------------- */

jQuery(function($) {
	"use strict";
$(".optin-section, .sidebar-optin, .footer-optin").each(function() {
    var $this = $(this);
    $this.html($this.html().replace(/&nbsp;/g, ''));
});
	});

jQuery(function($) {
	"use strict";
$('#credits-trigger, #credits-close').on('click', function(event){
			event.preventDefault();
			document.getElementById('fade').style.display='block';
			// create menu variable
    	var creditsPop = $('#credits-pop');
			var insideAnchor = $('#credits-pop section a');
    	// toggle open class

			if (creditsPop.hasClass("open")) {
				creditsPop.removeClass("open");
				document.getElementById('fade').style.display='none';
			} else {
				creditsPop.addClass("open");
			}
    });
	   });

/*  -------------------------------------------------------------
		DEFAULT JS FUNCTIONS BELOW THIS LINE
------------------------------------------------------------- */
/*SLIDE OUT MENU*/	
jQuery(function($) {
		// Slideout Menu
	"use strict";
    $('#slideout-trigger').on('click', function(event){
    	event.preventDefault();
    	// create menu variables
    	var slideoutMenu = $('#slideout-menu');
    	var slideoutMenuWidth = $('#slideout-menu').outerWidth();

    	// toggle open class
    	slideoutMenu.toggleClass("open");

    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.animate({
		    	right: "0px"
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	right: -slideoutMenuWidth
	    	}, 250);
    	}
    });

		$('#nav-close').on('click', function(event){
			event.preventDefault();
			// create menu variables
    	var slideoutMenu = $('#slideout-menu');
    	var slideoutMenuWidth = $('#slideout-menu').outerWidth();

    	// toggle open class
    	slideoutMenu.toggleClass("open");

    	// slide menu
    	if (slideoutMenu.hasClass("open")) {
	    	slideoutMenu.animate({
		    	right: "0px"
	    	});
    	} else {
	    	slideoutMenu.animate({
		    	right: -slideoutMenuWidth
	    	}, 250);
    	}
    });
	
	/*SLIDE OUT MENU DROPDOWN*/	
    $('#slideout-menu ul ul').hide();
    if ($('#slideout-menu .menu-item-has-children').length > 0) {
        $('#slideout-menu .menu-item-has-children').click(

        function () {
            $(this).addClass('toggled');
            if ($(this).hasClass('toggled')) {
                $(this).children('ul').slideToggle();
            }
            //return false;

        });
    }
});

/*PREVENT MENU JUMP*/
jQuery(function($) {
	'use strict';
	$(window).load(function() {
  var headwrap = $('#header-wrapper');

  if ($(headwrap).hasClass('sticky-menu')) {
    var wrapheight = $(headwrap).outerHeight();
    $(headwrap).css('position', 'fixed');
    $('body').css('padding-top', wrapheight);
    
    if ($('body').hasClass('admin-bar')) {
      var adminheight = $('#wpadminbar').outerHeight();
      
          $(headwrap).css('top', adminheight);
    } else {
          $(headwrap).css('top', '0');
    }
  }
});
});

/*HERO SLIDER*/
jQuery(function($) {
	'use strict';
    var el, set, timeRemain, sliderContinue;

    // App
    var Application = {
        settings: {
            sliderAutoplaySpeed: 7000,
            sliderSpeed: 1200
        },
        elements: {
            slider: $('#hero-slider'),
            slickAllThumbs: $('.slick-dots button'),
            slickActiveThumb: $('.slick-dots .slick-active button'),          
        },
        init: function() {
            set = this.settings;
            el = this.elements;
            this.slider();
        },

        slider: function() {
            el.slider.on('init', function() {
                $(this).find('.slick-dots button').text('');
                Application.dotsAnimation();
            });

            el.slider.slick({
                arrows: false,
                dots: true,
                autoplay: true,
                autoplaySpeed: set.sliderAutoplaySpeed,
                fade: true,
                speed: set.sliderSpeed,
                pauseOnHover: false,
                pauseOnDotsHover: true
            });

            $('.slick-dots').hover(
                function() {
                    var trackWidth = $('.slick-dots .slick-active button').width();
                    $('.slick-dots .slick-active button').stop().width(trackWidth);
                    el.slider.slick('slickPause');
                    clearTimeout(sliderContinue);
                },
                function() {
                    Application.dotsAnimation(timeRemain);
                    var trackWidth = $('.slick-dots .slick-active button').width();
                    sliderContinue = setTimeout(function() {
                        el.slider.slick('slickNext');
                        el.slider.slick('slickPlay');
                    }, timeRemain);
                }
            );

            el.slider.on('beforeChange', function() {
                $('.slick-dots button').stop().width(0);
            });

            el.slider.on('afterChange', function() {
                $('.slick-dots button').width(0);
                Application.dotsAnimation();
            });
        },

        dotsAnimation: function(remain) {
            if (remain) {
                var newDuration = remain;
            } else {
                var newDuration = set.sliderAutoplaySpeed;
            }
            $('.slick-dots .slick-active button').animate({ width: '100%' },
                {
                    duration: newDuration,
                    easing: 'linear',
                    step: function(now, fx) {
                        var timeCurrent = Math.round((now*set.sliderAutoplaySpeed)/100);
                        timeRemain = set.sliderAutoplaySpeed - timeCurrent;
                    }
                }
            );
        },
    };

    //Init
    Application.init();
    
    $(window).load(function() {
      $('.slick-slide .slider-image').height($(window).height());
    });
  
    $(window).resize(function() {
      $('.slick-slide .slider-image').height($(window).height());
    });

});


/*ADD FRAME AROUND WOOCOMMERCE IMAGES*/
/*jQuery(document).ready(function($){
	"use strict";
	$('.images .attachment-shop_single').wrapAll('<div class="frame"></div>');
});


/*PORTFOLIO ITEMS*/		
jQuery(document).ready(function ($) {
	"use strict";	
	var $container = $( '#portfolio .items').imagesLoaded(function(){
		$container.isotope({
  itemSelector: '.item',
  percentPosition: true,
  layoutMode: 'fitRows',
  fitRows: {
  	gutter: '.gutter-sizer'
  }
});
		});
	$('.filter li a').click(function(){
	$('.filter li a').removeClass('active');
	$(this).addClass('active');
		
		var selector = $(this).attr('data-filter');
				$container.isotope({ filter: selector });
		
		return false;
	});
	
	
 /*PORTFOLIO MASONRY GALLERY*/	   	
	var $masonry_container = $('.masonry-container').imagesLoaded(function(){
			$masonry_container.isotope({
  itemSelector: '.grid-item',
  percentPosition: true,
  masonry: {
    columnWidth: '.grid-sizer',
	gutter: 10
  }
});
	});
	
});

jQuery(document).ready(function ($) {
		"use strict";
	$(".fancybox").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
		
jQuery(document).ready(function ($) {
		"use strict";
		$('.items li').mouseenter(function() {
		$(this).children('a').children('div').fadeIn(300);
				}).mouseleave(function() {
		$(this).children('a').children('div').fadeOut(200);
				});
});


/*TESTIMONIALS SLIDER*/
jQuery(document).ready(function ($) {
	'use strict';
	$('.testimonials-container').slick({
		arrows:false,
		dots: true,
		slidesToShow: 1,
  		slidesToScroll: 1,
  		autoplay: true,
  		autoplaySpeed: 5000,
		infinite: true,
		speed: 300,
		fade: true,
  		cssEase: 'linear',
		adaptiveHeight: false
	});
});

jQuery(document).ready(function ($) {
	'use strict';
$('p').each(function(){
    $(this).html($(this).html().replace(/&nbsp;/gi,''));
});
	});