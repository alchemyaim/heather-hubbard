        <div style="clear:both;"></div>

    </div><!--CONTAINER-->

</div><!--OUTER WRAPPER-->

<footer>
<?php //WIDGETIZED FOOTER
	if(get_field('widgetized_footer_area', 'option') == "true") { ?>
	<div id="top-footer">
		<div class="row">
			<div class="container">
			
			<?php

// check if the repeater field has rows of data
if( have_rows('footer_areas', 'option') ):

 	// loop through the rows of data
    while ( have_rows('footer_areas', 'option') ) : the_row();
		
		//vars
		$size = get_sub_field('footer_area_size');
		$widget = get_sub_field('footer_widget'); ?>

       <div class="<?php echo $size; ?> columns">
				<?php if(is_active_sidebar(''.$widget.'')) dynamic_sidebar(''.$widget.''); ?>
				</div>

  <?php  endwhile;

else :

    // no rows found

endif;

?>
				
			</div>
		</div>
	</div>
<?php } ?>

	<div id="credits" class="container">
		<div class="center">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All rights reserved. <a href="<?php echo home_url(); ?>/privacy-policy" target="_self">Privacy Policy</a> | <a href="<?php echo home_url(); ?>/terms-of-use" target="_self">Terms of Use</a> | <a id="credits-trigger" href="#"><?php the_field('popup_box_title', 'options'); ?></a> | Theme by <a href="http://www.alchemyandaim.com/" target="_blank">Alchemy + Aim</a></div>
	</div>
	
</footer>


<div id="credits-pop" class="">
      <div class="inner">
        <a href="#" id="credits-close"></a>
        <h1><?php the_field('popup_box_title', 'options'); ?></h1>
          <section>
            <div>
             	<?php the_field('popup_content', 'options'); ?>
            </div>
          </section>
               
              </div>
    </div>
    <div id="fade" class="white_overlay"></div>
    
    
</div>


<?php wp_footer(); ?>

<?php //Google Analytics
	if(get_field('google_analytics_location', 'option') == "footer") {
    	echo get_field('google_analytics_code', 'option');
	}
?>

<?php //Custom Javascript
	$js = get_field('custom_javascript' ,'option');
	if( !empty($js) ): ?>
    	<script>
		(function($) {
        <?php echo $js; ?>
		})(jQuery);
    	</script>
<?php endif; ?>

<?php //STICKY NAV
	$image = get_field('site_header_logo', 'option');
	$image2 = get_field('site_header_logo_alt', 'option');

	if(get_field('sticky_nav', 'option') == "true") { ?>
<script>
	jQuery(document).ready(function($){
	/*STICKY MENU*/
    function init() {
		"use strict";
        window.addEventListener('scroll', function(){
            var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                shrinkOn = 1,
                header = document.querySelector("#header-wrapper");
            if (distanceY > shrinkOn) {
                classie.add(header,"smaller");
            } else {
                if (classie.has(header,"smaller")) {
                    classie.remove(header,"smaller");
                }
            }
        });
    }
    window.onload = init();
		});
	
	
	jQuery(window).scroll(function() {    
    var scroll = jQuery(window).scrollTop();

     //>=, not <=
    if (scroll >= 2) {
        //clearHeader, not clearheader - caps H
        jQuery("#linkbar").addClass("smaller");
		jQuery('#header-wrapper .logo').attr('src','<?php echo $image['url']; ?>');
	} else {
		jQuery("#linkbar").removeClass("smaller");
		jQuery('#header-wrapper .logo').attr('src','<?php if ( is_single() ) { echo $image['url']; } else { echo $image2['url']; }?>');
    }
}); 
	
</script>
	<?php } ?>
	
	</body>
</html>