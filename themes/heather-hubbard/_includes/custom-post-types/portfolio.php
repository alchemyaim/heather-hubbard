<?php

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM POST TYPE REGISTRATION
/*-----------------------------------------------------------------------------------*/

// Creates Movie Reviews Custom Post Type
function portfolio_init() {
    $args = array(
      'label' => 'Portfolio',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'portfolio'),
        'query_var' => true,
        'menu_icon' => 'dashicons-art',
        'supports' => array(
            'title',
            'revisions',
            'thumbnail',
            'author')
        );
    register_post_type( 'portfolio', $args );
}
add_action( 'init', 'portfolio_init' );

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM INTERACTION MESSAGES (optional)
/*-----------------------------------------------------------------------------------*/

function portfolio_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['portfolio'] = array(
    0 => '', 
    1 => sprintf( __('Portfolio Item updated. <a href="%s"> View video page.</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Portfolio Item updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Portfolio Item restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Portfolio Item published. <a href="%s">View video page.</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Portfolio Item saved.'),
    8 => sprintf( __('Portfolio Item submitted. <a target="_blank" href="%s">Preview video page</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Portfolio Item scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview video page</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Portfolio Item draft updated. <a target="_blank" href="%s">Preview video page</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'portfolio_updated_messages' );


/*-----------------------------------------------------------------------------------*/
/*  CONTEXTUAL HELP (optional)
/*-----------------------------------------------------------------------------------*/

function portfolio_contextual_help( $contextual_help, $screen_id, $screen ) { 
  if ( 'edit-portfolio' == $screen->id ) {

    $contextual_help = '<h2>Portfolio</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce est libero, congue eget gravida mollis, eleifend sit amet felis.</p> 
    <p>Sed mollis pretium dolor at vestibulum. Phasellus condimentum dui in velit interdum, sed aliquam ex hendrerit.</p>';

  } elseif ( 'portfolio' == $screen->id ) {

    $contextual_help = '<h2>Editing Portfolio</h2>
    <p>Nunc eleifend arcu sit amet tortor <strong>luctus aliquam.</strong> Sed a massa vestibulum, iaculis magna ac, faucibus tellus.</p>';

  }
  return $contextual_help;
}
add_action( 'contextual_help', 'portfolio_contextual_help', 10, 3 );

/*-----------------------------------------------------------------------------------*/
/*  CUSTOM TAXONOMIES
/*-----------------------------------------------------------------------------------*/

function portfolio_taxonomies() {
  $labels = array(
    'name'              => _x( 'Portfolio Item Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Portfolio Item Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Portfolio Item Categories' ),
    'all_items'         => __( 'All Portfolio Item Categories' ),
    'parent_item'       => __( 'Parent Portfolio Item Category' ),
    'parent_item_colon' => __( 'Parent Portfolio Item Category:' ),
    'edit_item'         => __( 'Edit Portfolio Item Category' ), 
    'update_item'       => __( 'Update Portfolio Item Category' ),
    'add_new_item'      => __( 'Add New Portfolio Item Category' ),
    'new_item_name'     => __( 'New Portfolio Item Category' ),
    'menu_name'         => __( 'Portfolio Item Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'portfolio_category', 'portfolio', $args );
}
add_action( 'init', 'portfolio_taxonomies', 0 );


/*
//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_item_types_hierarchical_taxonomy', 0 );

function create_item_types_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

  $labels = array(
    'name' 				=> _x( 'Portfolio Item Type', 'taxonomy general name' ),
    'singular_name' 	=> _x( 'Portfolio Item Type', 'taxonomy singular name' ),
    'search_items' 		=>  __( 'Search Portfolio Item Type' ),
    'all_items' 		=> __( 'All Portfolio Item Type' ),
    'parent_item' 		=> __( 'Parent Portfolio Item Type' ),
    'parent_item_colon' => __( 'Parent Portfolio Item Type:' ),
    'edit_item' 		=> __( 'Edit Portfolio Item Type' ), 
    'update_item' 		=> __( 'Update Portfolio Item Type' ),
    'add_new_item' 		=> __( 'Add Portfolio Item Type' ),
    'new_item_name' 	=> __( 'New Portfolio Item Type Name' ),
    'menu_name' 		=> __( 'Portfolio Item Type' ),
  ); 	

// Now register the taxonomy

  register_taxonomy('types',array('portfolio'), array(
    'hierarchical' 		=> true,
    'labels' 			=> $labels,
    'show_ui' 			=> true,
    'show_admin_column' => true,
    'query_var' 		=> true,
    'rewrite' 			=> array( 'slug' => 'type' ),
  ));

}
*/

/* THIS HELPS FOR SINGLE PAGES FOR CUSTOM POST TYPES */
flush_rewrite_rules();
?>