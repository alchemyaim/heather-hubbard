<?php

//Add an action that will load all widgets
add_action( 'widgets_init', 'load_widgets' );

//Function that registers the widgets
function load_widgets() {
	register_widget('social_widget');
	register_widget('rich_text_widget');
	register_widget('rotator1_widget');
	register_widget('rotator2_widget');
	register_widget('form_widget');
	register_widget('footer_form_widget');
	
}


/*---------------------------------------------
	POPULAR POSTS WIDGET (Sidebar Version)
----------------------------------------------*/

class rotator1_widget extends WP_Widget {
	
	function rotator1_widget (){
		
		$widget_ops = array( 'classname' => 'posts', 'description' => 'A widget that displays the most popular posts on your blog(sidebar version)' );
		$control_ops = array( 'width' => 250, 'height' => 120, 'id_base' => 'popular1-widget' );
		$this->WP_Widget( 'popular1-widget', '  Popular Posts #1', $widget_ops, $control_ops );
		
	}
		
	function widget($args, $instance){
			
		extract($args);
			
		$title = apply_filters('widget_title', $instance['title']);
		$no = $instance['no'];
		global $post;		
		echo $before_widget;		
		echo $before_title.$title.$after_title;
		echo '<ul class="posts-list">';
					
		$no_limit = 0;
			
			$all_posts = get_posts( array('numberposts' => $no, 'meta_key' => 'post_views_count', 'orde y' => 'meta_value_num', 'order' => 'DESC') );
			foreach($all_posts as $post) : setup_postdata($post); ?>

				<?php if($no_limit++ < $no) : ?>

					<li class="popular">
						<div class="featured"><a href="<?php the_permalink(); ?>"><div class="postthumb"><?php the_post_thumbnail('pop-posts-image'); ?></div></a></div>
						<div class="meta"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
					</li>

				<?php endif; ?>
			<?php endforeach;
		echo '</ul>';
		echo $after_widget;
			
	}
			
	function update($new_instance, $old_instance){
		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['no'] = strip_tags($new_instance['no']);
			
		return $instance;
			
	}
		
	function form($instance){
	
		$defaults = array( 'title' => 'Popular Posts', 'no' => '3' );
		$instance = wp_parse_args((array) $instance, $defaults);
			
		?>
			
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'whitelabel'); ?></label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
			</p>
			
			<p>
				<label for="<?php echo $this->get_field_id( 'no' ); ?>"><?php _e('Show number of posts:', 'whitelabel'); ?></label>
				<input id="<?php echo $this->get_field_id( 'no' ); ?>" name="<?php echo $this->get_field_name( 'no' ); ?>" value="<?php echo $instance['no']; ?>" style="width:100%;" />
			</p>
		<?php	
	}	
}

/*---------------------------------------------
	POPULAR POSTS WIDGET (Footer Version)
----------------------------------------------*/
class rotator2_widget extends WP_Widget {
	
	function rotator2_widget (){
		
		$widget_ops = array( 'classname' => 'posts', 'description' => 'A widget that displays the most popular posts on your blog(footer version)' );
		$control_ops = array( 'width' => 250, 'height' => 120, 'id_base' => 'popular2-widget' );
		$this->WP_Widget( 'popular2-widget', '  Popular Posts #2', $widget_ops, $control_ops );
		
	}
		
	function widget($args, $instance){
			
		extract($args);
			
		$title = apply_filters('widget_title', $instance['title']);
		$no = $instance['no'];
			
		global $post;
			
		echo $before_widget;
			
		echo $before_title.$title.$after_title;

		echo '<ul class="posts-list">';
					
		$no_limit = 0;
			
			$all_posts = get_posts( array('numberposts' => $no, 'meta_key' => 'post_views_count', 'orde y' => 'meta_value_num', 'order' => 'DESC') );

			foreach($all_posts as $post) : setup_postdata($post); ?>

				<?php if($no_limit++ < $no) : ?>

					<li class="popular">
						<div class="featured"><a href="<?php the_permalink(); ?>"><div class="postthumb"><?php the_post_thumbnail('pop-posts-image'); ?></div></a></div>
						<div class="meta"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
					</li>

				<?php endif; ?>
				
			<?php endforeach;

			
		echo '</ul>';
			
		echo $after_widget;
			
	}
			
	function update($new_instance, $old_instance){
		
		$instance = $old_instance;
			
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['no'] = strip_tags($new_instance['no']);
			
		return $instance;
			
	}
		
	function form($instance){
		
		$defaults = array( 'title' => 'Popular Posts', 'no' => '3' );
			
		$instance = wp_parse_args((array) $instance, $defaults);
			
		?>
			
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'whitelabel'); ?></label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
			</p>
			
			<p>
				<label for="<?php echo $this->get_field_id( 'no' ); ?>"><?php _e('Show number of posts:', 'whitelabel'); ?></label>
				<input id="<?php echo $this->get_field_id( 'no' ); ?>" name="<?php echo $this->get_field_name( 'no' ); ?>" value="<?php echo $instance['no']; ?>" style="width:100%;" />
			</p>
			
		<?php
			
	}
		
}

/*--------------------
		FORM WIDGET
-----------------------*/
class form_widget extends WP_Widget {
	function form_widget (){
		$widget_ops = array( 'classname' => 'sidebar-optin', 'description' => 'A widget that displays an opt in form in your sidebar.' );
		$control_ops = array( 'width' => 250, 'height' => 120, 'id_base' => 'form-widget' );
		$this->WP_Widget( 'form-widget', 'Optin Form Widget', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		$text = get_field('opt_in_sub_text' , 'widget_' . $args['widget_id']); ?>
		
		
<div class="side-bar-form">
	<h2 class="form-title"><?php echo $title; ?></h2>
	<div class="form-text"><?php echo $text; ?></div>
	<?php the_field('form_code','options'); ?>
</div>


 
  
<?php

		echo $after_widget;
	}

	function update($new_instance, $old_instance){
		$instance = $old_instance;	
		$instance['title'] = strip_tags($new_instance['title']);					
		return $instance;
	}	

	function form($instance){
		$defaults = array( 'title' => 'Optin Form Widget');
		$instance = wp_parse_args((array) $instance, $defaults);
		
?>	
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'theme'); ?></label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
			</p>
			
<?php	
	}
}


/*--------------------
	FOOTER FORM WIDGET
-----------------------*/
class footer_form_widget extends WP_Widget {
	function footer_form_widget (){
		$widget_ops = array( 'classname' => 'footer-optin', 'description' => 'A widget that displays an opt in form in your footer.' );
		$control_ops = array( 'width' => 250, 'height' => 120, 'id_base' => 'footer-form-widget' );
		$this->WP_Widget( 'footer-form-widget', 'Optin Form Widget (Footer Version)', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		$text = get_field('footer_opt_in_sub_text' , 'widget_' . $args['widget_id']); ?>
		
		
<div class="side-bar-form">
	<h2 class="form-title"><?php echo $title; ?></h2>
	<div class="form-text"><?php echo $text; ?></div>
	<?php the_field('bp_form_code','options'); ?>
</div>


 
  
<?php

		echo $after_widget;
	}

	function update($new_instance, $old_instance){
		$instance = $old_instance;	
		$instance['title'] = strip_tags($new_instance['title']);					
		return $instance;
	}	

	function form($instance){
		$defaults = array( 'title' => 'Footer Optin Form Widget');
		$instance = wp_parse_args((array) $instance, $defaults);
		
?>	
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'theme'); ?></label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
			</p>
			
<?php	
	}
}

/*------------------
	SOCIAL WIDGET
---------------------*/


class social_widget extends WP_Widget {
	function social_widget (){
		$widget_ops = array( 'classname' => 'social', 'description' => 'A widget that displays your social links' );
		$control_ops = array( 'width' => 250, 'height' => 120, 'id_base' => 'social-widget' );
		$this->WP_Widget( 'social-widget', ' Social Widget', $widget_ops, $control_ops );
	}
	
	function widget($args, $instance){		
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		//echo $before_title.$title.$after_title;
			
		echo '<div class="social-links"><span class="social-title">'.$title. '</span>';
             
        if( have_rows('social_links', 'option') ): 
		while( have_rows('social_links', 'option') ): the_row(); 

		// vars
		$icon = get_sub_field('social_media_icon', 'option');
		$link = get_sub_field('social_link', 'option');
		
		if( $link ): ?>
				<a href="<?php echo $link; ?>" target="_blank" class="social-link">
		<?php endif; ?>
        		<i class="fa <?php echo $icon; ?>"></i>
		<?php if( $link ): ?>
				</a>
		<?php endif; 
			  endwhile; 
			  endif; 
			  
		echo '</div>';
			
		echo $after_widget;
	}
			
	function update($new_instance, $old_instance){
		$instance = $old_instance;	
		$instance['title'] = strip_tags($new_instance['title']);					
		return $instance;
	}		
		
	function form($instance){
		$defaults = array( 'title' => 'Social Links');
		$instance = wp_parse_args((array) $instance, $defaults);
		
?>	
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'theme'); ?></label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
			</p>
			
			<p>
				Edit social links within the <a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=theme-general-settings">Theme Options Panel</a>.
			</p>
<?php	
	
	}
}



/*--------------------
	WYSIWYG WIDGET
---------------------*/
class rich_text_widget extends WP_Widget {
	function rich_text_widget (){
		$widget_ops = array( 'classname' => 'rich_text', 'description' => 'A rich text WYSIWYG widget.' );
		$control_ops = array( 'width' => 250, 'height' => 120, 'id_base' => 'rich-text-widget' );
		$this->WP_Widget( 'rich-text-widget', 'WYSIWYG Widget', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		echo $before_title.$title.$after_title;
		
		echo get_field('wysiwyg_widget', 'widget_' . $args['widget_id']);
?>
<p>&nbsp;</p>
<?php
		echo $after_widget;
	}

	function update($new_instance, $old_instance){
		$instance = $old_instance;	
		$instance['title'] = strip_tags($new_instance['title']);					
		return $instance;
	}	

	function form($instance){
		$defaults = array( 'title' => 'WYSIWYG Widget');
		$instance = wp_parse_args((array) $instance, $defaults);
		
?>	
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'theme'); ?></label>
				<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" style="width:100%;" />
			</p>
			
<?php	
	}
}
?>