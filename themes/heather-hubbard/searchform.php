<form method="get" id="search_form" action="<?php bloginfo('home'); ?>/">
	<input type="text" class="search_input" value="Search..." name="s" id="s" onfocus="if (this.value == 'Search...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search...';}" />
	<input type="hidden" id="searchsubmit" value="Search" />
	<button id="search-submit" class="button button-primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
</form>
