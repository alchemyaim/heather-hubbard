<?php
/**
 * Portfolio Single Page
 */

get_header();
?>

<div class="container">
    <div class="row">
    <div class="seven columns">
    
    	<?php 
			//var
			$gallery = get_field('image_gallery');
			$vid_id = get_field('video_id');
			$vid = get_field('self_hosted_video');
			$audio = get_field('self_hosted_audio');
			$sc = get_field('soundcloud_id');
		?>
		
		<?php if(get_field('type_of_media') == "images") { ?>
		<?php if( !empty($gallery) ): ?>
			
			<div class="masonry-container">
			<div class="grid-sizer"></div>
				<?php foreach( $gallery as $image ): ?>
					<div class="grid-item">
						<a href="<?php echo $image['url']; ?>" class="fancybox" rel="gallery1">
							 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a> 
					</div>
				<?php endforeach; ?>
			</div>	 
				 <?php else: ?>
				 <?php the_post_thumbnail('full'); ?>
			<?php endif ?>
			<?php } ?>
			
			<?php if(get_field('type_of_media') == "video") { ?>
			<?php if( !empty($vid) ): ?>
			<?php echo '<video controls><source src="'. $vid .'" type="video/mp4"></video>'; ?>
			<?php endif ?>
			<?php if( !empty($vid_id) ): ?>
			<?php if(get_field('video') == "vimeo") { ?>
		<?php echo '<div class="video-container"><iframe src="http://player.vimeo.com/video/'. $vid_id .'" width="960" height="540" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>'; ?>
		<?php } else if(get_field('video') == "youtube") { ?>
		<?php echo '<div class="video-container"><iframe width="960" height="540" src="http://www.youtube.com/embed/'. $vid_id .'?rel=0&vq=hd1080;3&amp;autohide=1&amp;&amp;showinfo=0" frameborder="0" allowfullscreen></iframe></div>'; ?>
		
		<?php } ?>
		<?php endif ?>
			<?php } ?>
			
			<?php if(get_field('type_of_media') == "audio") { ?>
			<?php if( !empty($audio) ): ?>
			<?php echo do_shortcode('[audio src="'. $audio .'"]'); ?>
			<?php endif ?>
			<?php if( !empty($sc) ): ?>
			<?php if(get_field('audio') == "soundcloud") { ?>
		<?php echo do_shortcode('[soundcloud id="'. $sc .'" comments="true" autoplay="false" playertype="Standard" width="100%" color="#aac2a0"]'); ?>
	
		<?php } ?>
		<?php endif ?>
			<?php } ?>
			
		</div>
		<div class="five columns">
			<div class="project-desc">
			<h2><?php the_title(); ?></h2>
			<h5>Project Description</h5>
			<?php the_field('project_description'); ?>
			</div>
			
			<div class="project-divider"></div>
			
			<div class="project-details">
			<h6>Project Details</h6>
			<?php 
				//var
				$client = get_field('client');
				$date = get_field('project_date');
				$skills = get_field('skills');
				$link = get_field('client_link');
				$data = str_replace('http://', '' , $link);
			?>
			
			<?php if( !empty($client) ): ?>
				<div class="client-detail">CLIENT: <?php echo $client; ?></div>
			<?php endif ?>
			<?php if( !empty($date) ): ?>
				<div class="date-detail">DATE: <?php echo $date; ?></div>
			<?php endif ?>
			<?php if( !empty($skills) ): ?>
				<div class="skills-detail">SKILLS: <?php echo $skills; ?></div>
			<?php endif ?>
			<?php if( !empty($link) ): ?>
				<div class="link-detail">LINK: <a href="<?php echo $link; ?>" target="_blank"><?php echo $data; ?></a></div>
			<?php endif ?>
			</div>
			
		</div>
	</div>
	
	<hr>
	
	<div class="project-related">
	<h5>Related Projects</h5>
   	<div id="portfolio" class="portfolio">
   		<ul class="items related-portfolio">
   		<div class="gutter-sizer"></div>
   			<?php
				$tags = array();
				$post_terms = get_the_terms($post->ID, 'portfolio_category' );
				$tag = isset($post_terms[0] -> name) ? $post_terms[0] -> name : '';
				if(!empty($post_terms)){
					if(!is_wp_error( $post_terms ))
						foreach($post_terms as $term)
							array_push($tags, $term->name); 
				}
		
				$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
				$args = array( 'posts_per_page' => 3, 
					   'offset'=> 0,
					   'portfolio_category' => implode($tags, ','));

				$all_posts = new WP_Query($args);
				while($all_posts->have_posts()) : $all_posts->the_post();
					
			?>
				
			<li class="item"><a href="<?php the_permalink(); ?>">
				<div class="caption">
						<h3><?php the_title(); ?></h3>
						<span class="short-desc"><?php the_field('project_short_description'); ?></span>
						<span class="icon <?php the_field('type_of_media') ?>"></span>
					</div>
					<?php the_post_thumbnail('portfolio-image'); ?>
			</a></li>

			<?php endwhile; ?>
   		</ul>
   	</div>
	</div>
  
  	<div class="portfolio-nav">
			<div class="nav-older"><?php previous_post_link('%link', 'Previous Project') ?></div>
			<div class="nav-newer"><?php next_post_link('%link', 'Next Project') ?></div>
	</div>
	   		
   	
</div> <!-- /.container -->

<?php get_footer(); ?>
